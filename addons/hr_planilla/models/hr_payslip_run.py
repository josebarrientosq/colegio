from odoo import api, fields, models, _
import codecs
from odoo.exceptions import UserError, ValidationError
import os

class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'

    period_id = fields.Many2one('account.period', "Periodo", required=True)
    situacion = fields.Selection(selection=[("0", "BAJA"), ("1", "ACTIVO O SUBSIDIADO"), ("2", "SIN VINCULO LABORAL"),
                                            ("3", "SUSPENSION PERFECTA")], default='1')

    estructura = fields.Many2one('hr.payroll.structure', "Estructura", required=True)

    @api.onchange('period_id')
    def actualizar_fecha(self):
        self.date_start = self.period_id.date_start
        self.date_end = self.period_id.date_stop

    @api.multi
    def validar_planillas(self):
        for planilla in self.slip_ids:
            planilla.action_payslip_done()

    @api.multi
    def calcular_planillas(self):
        for planilla in self.slip_ids:
            planilla.set_period(self.period_id.id)
            planilla.ejecutar_calculos()

    @api.multi
    def calcular_cts_run(self):
         for planilla in self.slip_ids:
            planilla.get_cts()


    @api.multi
    def set_situacion(self):
         for planilla in self.slip_ids:
            planilla.set_situacion(self.situacion)

    @api.multi
    def exportar(self):

        file_data = self._get_txt()
        #LERRRRRRRRRRRAAAAMM0014010000OIM1.TXT
        file_name = "PLA_HAB" +  ".txt"
        values = {
            'name': file_name,
            'datas_fname': file_name,
            'res_model': 'ir.ui.view',
            'res_id': False,
            'type': 'binary',
            'public': True,
            'datas' : codecs.encode(codecs.encode(file_data,'utf8'),'base64'),

        }
        attachment_id = self.env['ir.attachment'].sudo().create(values)
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
        }

    @api.multi
    def _get_txt(self):
        i=1
        txt=""
        documents = self.slip_ids
        if len(documents)==0:
            raise UserError("No existen datos")

        for document in documents:
            if document.employee_id.cta_bancaria:

                txt +=  "2A"+str(document.employee_id.cta_bancaria).replace('-','')+"\t" +"1"+ str(document.employee_id.identification_id) +"\t" +str(document.employee_id.name)+"\t\t\t\t"+"0001"+"{:.2f}".format(document.neto).zfill(17)+"S"
                txt += "\n"
                i += 1

        return txt

#100008320190831XC00011941898142050       00000000086906.60HABERES AGOSTO  2019                    005229149445086
#2A19121018564071      144669199       AROTOMA DE LA CRUZ NORY SILVESTRA                                                                                                      000100000000001783.50S

    @api.multi
    def exportar_cts(self):

        file_data = self._get_txt_cts()
        #LERRRRRRRRRRRAAAAMM0014010000OIM1.TXT
        file_name = "CTS" +str(self.period_id.code) +  ".txt"
        values = {
            'name': file_name,
            'datas_fname': file_name,
            'res_model': 'ir.ui.view',
            'res_id': False,
            'type': 'binary',
            'public': True,
            'datas' : codecs.encode(codecs.encode(file_data,'utf8'),'base64'),

        }
        attachment_id = self.env['ir.attachment'].sudo().create(values)
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
        }

    @api.multi
    def _get_txt_cts(self):
        i = 1
        txt = ""
        planillas = self.slip_ids
        if len(planillas) == 0:
            raise UserError("No existen datos")

        for pla in planillas:
            for linea in pla.input_line_ids:
                if linea.name=='CTS':
                    txt += "2A" + str(pla.employee_id.cta_bancaria).replace('-', '') + "\t" + "1" + str(
                        pla.employee_id.identification_id) + "\t" + str(pla.employee_id.name) + "\t\t\t\t" + "0001" + "{:.2f}".format(pla.neto).zfill(17) + "0001" + "{:.2f}".format(pla.ingresos*6).zfill(17)
                    txt += "\n"
                    i += 1

        return txt