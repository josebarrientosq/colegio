# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Afiliaciones (models.Model):
    _name = "hr_planilla.afiliaciones"
    _description = "Tipos de afiliaciones"

    name = fields.Char()
    fondo_pension = fields.Float("Tasa Fondo Pension (%)")
    prima_seguro = fields.Float("Prima de seguro (%)")
    comision_porcentual = fields.Float("Comisión porcentual (%)")
    total= fields.Float("Total (%)", compute='get_total')

    @api.one
    def get_total(self):
        self.total= self.fondo_pension + self.prima_seguro + self.comision_porcentual

class Aportaciones_empleador (models.Model):
    _name = "hr_planilla.aportes"
    _description = "Tipos de aportes"

    name= fields.Char()
    tasa = fields.Float("Tasa(%)")