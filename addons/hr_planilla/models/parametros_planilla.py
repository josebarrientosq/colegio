from odoo import api, fields, models


class Parametros_planilla(models.TransientModel):
    _inherit = 'res.config.settings'

    rmv =fields.Float(string='RMV')
    asig_fam=fields.Float(string='Asignacion familiar')
    essalud=fields.Float(string='ESSALUD')

    @api.model
    def get_values(self):
        conf = self.env['ir.config_parameter']
        return {
            'rmv': float(conf.get_param('rmv')),
            'asig_fam': float(conf.get_param('asig_fam')),
            'essalud': float(conf.get_param('essalud')),

        }

    @api.one
    def set_values(self):
        conf = self.env['ir.config_parameter']

        conf.set_param('rmv', self.rmv)
        conf.set_param('asig_fam', self.asig_fam)
        conf.set_param('essalud', self.essalud)

class uit_parametros(models.Model):
    _name = 'hr_planilla.parametro_uit'
    _description = 'Parametros UIT'

    fiscalyear_id = fields.Many2one('account.fiscalyear','Año Fiscal')
    uit = fields.Float('Total UIT')

class renta_quinta_parametros(models.Model):
    _name = 'hr_planilla.parametro_renta_quinta'
    _description = 'Parametros Renta de 5ta categoria'

    nivel =fields.Integer('nivel')
    limite_inferior = fields.Float('Limite inferior en UIT')
    limite_superior = fields.Float('Limite superior en UIT')
    tasa = fields.Float('Tasa %')