# -*- coding: utf-8 -*-

from . import models
from . import hr_employee
from . import parametros_planilla
from . import afiliaciones
from . import hr_payslip
from . import hr_payslip_run
from . import hr_holidays
from . import hr_contract
from . import hr_renta_quinta
from . import hr_gratificacion
from . import res_company
from . import hr_salary_rule
from . import hr_cts
#from . import rmv