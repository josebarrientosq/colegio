//planilla
function login(){
    console.log("login");
    var data = {
        "dni" : $("#dni").val(),
        "password" : $("#password").val(),
    }

    $.post("/autenticador", data).done(function(response) {

        console.log(response)
        if (response =='dni'){
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>El DNI es incorrecto</div>")
        }
        else if (response =='clave') {
            $("#respuesta").html("<div class='alert alert-danger' role='alert'>La clave es incorrecta</div>")
            }
            else{
            $("#principal").html(response)
            buscar_planillas()
            }
    })
}

function buscar_planillas() {
    console.log("buscando planillas");
    var data = {
            "fecha_inicio":$("#fecha_inicio").val(),
            "fecha_fin":$("#fecha_fin").val(),
            "dni": $("#codigo").text()
    }
    console.log($("#codigo").text());

    $.post("/mostrarplanilla", data).done(function(response) {
        $("#documento").html(response)
    })
}

function buscar_rh() {
    console.log("buscando rh");
    var data = {
            "fecha_inicio":$("#fecha_inicio").val(),
            "fecha_fin":$("#fecha_fin").val(),
            "dni": $("#codigo").text()
    }
    console.log($("#codigo").text());

    $.post("/mostrar_rh", data).done(function(response) {
        $("#documento").html(response)
    })
}

function buscar_asistencia() {
    console.log("buscando asistencia");

    var data = {
            "fecha_inicio":$("#fecha_inicio").val(),
            "fecha_fin":$("#fecha_fin").val(),
            "dni": $("#codigo").text()

    }

    $.post("/asistencia", data).done(function(response) {
        $("#documento").html(response)
    })
}


function buscar_faltas() {
    console.log("buscando faltas");
    var data = {
            "fecha_inicio":$("#fecha_inicio").val(),
            "fecha_fin":$("#fecha_fin").val(),

    }
    $.post("/faltas", data).done(function(response) {
        $("#documento").html(response)
    })
}

$(".btn_reclamo").click(function() {
    var $row = $(this).closest("tr");    // Find the row
    var $text = $row.find(".nr").text(); // Find the text
    var $reclamo = $row.find(".reclamo").text(); // Find the text

    document.getElementById("recipient-name").value = $text;
    document.getElementById("message-text").value = $reclamo;
});

$(".btn_rh").click(function() {
    console.log("obteniendo numeracion");
    var $row = $(this).closest("tr");    // Find the row
    var $number_rh = $row.find(".hr_number").text(); // Find the text

    document.getElementById("number_rh").value = $number_rh;
    document.getElementById("number_rh2").value = $number_rh;


});


function enviar_rh() {
    var image_input = null;
    var file = document.getElementById("archivo_rh").files[0];

    if (file) {
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function(e)
         {
             image_input = e.target.result;
         }
    }
    var data = {
            "number_rh":$("#number_rh").val(),
            "serie_rh":$("#serie_rh").val(),
            "archivo_rh" : image_input,
    }

    $('.modal-backdrop').remove(); //remover fondo negro
    $('body').css({ overflow: 'visible'}); //permitir scroll

    $.post("/rh/attachment/add", data).done(function(response) {
        $("#documento").html(response)
    })

}


$(function(){


    $('form').submit(function(event){
        event.preventDefault()
        console.log("submi")
  })


});