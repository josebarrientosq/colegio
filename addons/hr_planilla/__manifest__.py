# -*- coding: utf-8 -*-
{
    'name': "hr_planilla",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','hr_contract','hr_holidays','hr_payroll','account_period'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/hr_security.xml',
        'data/hr.salary.rule.category.xml',
        'data/hr.salary.rule.xml',
        'data/hr.holidays.status.xml',
        'data/afiliaciones_data.xml',
        'data/aportaciones_data.xml',
        'data/hr_contract_data.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/hr_employee_view.xml',
        'views/hr_payslip_views.xml',
        'views/parametros_view.xml',
        'views/afiliaciones_view.xml',
        'views/hr_holidays_view.xml',
        'views/hr_renta_quinta_view.xml',
        'views/hr_gratificaciones_view.xml',
        'views/hr_cts_view.xml',
        'views/res_company_view.xml',
        'views/hr_salary_rule_views.xml',
        'report/report_planilla_template.xml',
        'wizard/contrato_fecha_cese_wizard_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}