# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug.utils
from odoo.addons.web.controllers.main import Database
import unicodedata
import os
import base64

class consulta_cpe(http.Controller):

    fecha_inicio= None
    fecha_fin = None
    nombre = None
    dni=None

    @http.route('/planilla', auth='public')
    def login(self, **kwargs):
        return request.render('hr_planilla.login')


    @http.route('/autenticador', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def autenticar(self, **post):
        dni = post.get("dni")
        password = post.get("password")

        empleado= request.env["hr.employee"].sudo().search([('identification_id','=',dni)])

        if empleado:

            nombre = empleado.name

            if empleado.clave == password or empleado.clave == '' or empleado.clave is False or True:
                return http.request.render('hr_planilla.principal', {'nombre': nombre, 'dni': dni})
            else:
                return 'clave'
        else:
            return 'dni'


    @http.route('/mostrarplanilla', type='http', auth='public', method=["POST"], csrf=False)
    def mostrar_planillas(self, **post):

        self.dni = post.get("dni")
        self.fecha_inicio = post.get("fecha_inicio")
        self.fecha_fin = post.get("fecha_fin")
        empleado = request.env["hr.employee"].sudo().search([('identification_id','=',self.dni)], limit=1)

        if self.fecha_inicio and self.fecha_fin:
            planilla = request.env["hr.payslip"].sudo().search([ ('employee_id', '=', empleado.id),('date_from','>=',self.fecha_inicio),('date_to','<=',self.fecha_fin)])
        else:
            planilla = request.env["hr.payslip"].sudo().search([('employee_id', '=', empleado.id)])
        return request.render('hr_planilla.boletas_pago', {'documento': planilla})


    @http.route("/consulta/planilla/pdf/<identificador>", auth='public', type='http', method=["GET"], csrf=False)
    def get_pdf(self, identificador):

        planilla = request.env["hr.payslip"].sudo().search([ ('number', '=', identificador)])
        os.system("echo '%s'" % (planilla))
        if planilla:
            pdf = request.env.ref('hr_planilla.planilla_detalle_report').sudo().render_qweb_pdf([planilla.id])[0]
            pdfhttpheaders = [('Content-Type', 'application/pdf'),
                              ('Content-Length', len(pdf)),
                              ('Content-Disposition', 'attachment; filename="%s.pdf"' % (planilla.number))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect('/')

    @http.route('/mostrar_rh', type='http', auth='public', method=["POST"], csrf=False)
    def mostrar_rh(self, **post):
        dni = post.get("dni")
        fecha_inicio = post.get("fecha_inicio")
        fecha_fin = post.get("fecha_fin")
        empleado = request.env["hr.employee"].sudo().search([('identification_id','=',dni)], limit=1)

        if fecha_inicio and fecha_fin:
            rh = request.env["hr.payslip"].sudo().search([ ('employee_id', '=', empleado.id),('date_from','>=',fecha_inicio),('date_to','<=',fecha_fin)])
        else:
            rh = request.env["hr.payslip"].sudo().search([('employee_id', '=', empleado.id)])
        return request.render('hr_planilla.recibos_honorarios', {'documento': rh})


    @http.route('/payslip/attachment/add', type='http', auth="public", website=True)
    def payslip_attachment_add(self, url=None, upload=None, **post):

        cr, uid, context = request.cr, request.uid, request.context
        number_rh = post.get("number_rh")
        os.system("echo '%s'" % (number_rh))
        payslip = request.env["hr.payslip"].sudo().search([('number', '=', number_rh)])

        Attachments = request.env['ir.attachment']  # registry for the attachment table

        upload_file = request.httprequest.files.getlist('upload')
        # data = upload_file.read()
        if upload_file:
            for i in range(len(upload_file)):
                attachment_id = Attachments.create({
                    'name': upload_file[i].filename,
                    'type': 'binary',
                    'datas': base64.b64encode(upload_file[i].read()),
                    'datas_fname': upload_file[i].filename,
                    'public': True,
                    'res_model': 'ir.ui.view',
                    'payslip_attachment_id': payslip.id,
                })

            if self.fecha_inicio and self.fecha_fin:
                planilla = request.env["hr.payslip"].sudo().search(
                    [('employee_id.identification_id', '=', self.dni), ('date_from', '>=', self.fecha_inicio),
                     ('date_to', '<=', self.fecha_fin)])
            else:
                planilla = request.env["hr.payslip"].sudo().search([('employee_id.identification_id', '=', self.dni)])

            return request.redirect('/mostrar_rh')