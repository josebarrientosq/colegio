from odoo import models, fields, api
from odoo.exceptions import ValidationError, UserError


class Wizardcontrato(models.TransientModel):
    _name = 'wizard_contrato_cese'
    _description = "Colocar fecha de cese"

    fecha_cese = fields.Date('Fecha cese')

    @api.multi
    def set_fecha_cese(self):
        active_ids = self.env.context.get('active_ids')
        records = self.env['hr.contract'].browse(active_ids)
        for record in records:
            record.set_fecha_cese(self.fecha_cese)