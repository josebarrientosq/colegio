# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, api, fields, _
from odoo.exceptions import UserError
from odoo.tools.profiler import profile


class OpStudentFeesDetails(models.Model):
    _name = 'op.student.fees.details'
    _description = 'Student Fees Details'

    fees_line_id = fields.Many2one('op.fees.terms.line', 'Fees Line')
    invoice_id = fields.Many2one('account.invoice', 'Comprobante')
    amount = fields.Float('Monto Pagado')
    mora = fields.Float('Mora Pagada')
    #date_doc = fields.Date('Fecha de emisión')
    date_due = fields.Date('Fecha de Vencimiento')
    date_pay = fields.Date('Fecha de Pago')
    product1 = fields.Many2one('product.product', 'Product')
    product2 = fields.Many2one('product.product', 'Product')
    tax = fields.Many2one('account.tax')
    student_id = fields.Many2one('op.student', 'Student')
    DNI = fields.Char('DNI' , index=True)
    n_operacion = fields.Char('N operacion', size=6 )
    reference = fields.Char(string='Referencia')
    check = fields.Boolean(default=False)

    #state = fields.Selection([('draft', 'Borrador'), ('invoice', 'Comprobante Creado')], 'Status')
    type = fields.Selection([
        ('matricula', 'matricula'), ('pension', 'pension'), ('otros', 'otros')], 'Tipo')
    invoice_state = fields.Selection('State', related="invoice_id.state", readonly=True , store=True)


    @api.onchange('DNI')
    @api.constrains('DNI')
    def check_dni(self):
        if self.DNI and len(self.DNI)!= 8:
            raise UserError ("El DNI debe tener 8 digitos "+ self.DNI)
        student_id = self.env['op.student'].search([('vat', '=', self.DNI)])
        if not student_id :
            raise UserError("No se encuentra el alumno con el DNI : "+ self.DNI)

        if len(student_id)>1:
            raise UserError("Existen datos duplicados para el DNI : " + self.DNI)


    @api.multi
    @profile
    def get_invoice(self,tax,mora):
        """ Create invoice for fee payment process of student """

        inv_obj = self.env['account.invoice']

        student_id = self.env['op.student'].search([('vat', '=', self.DNI)])

        if not student_id :
            raise UserError("No se encuentra el alumno con el DNI : " + self.DNI)

        if len(student_id)>1:
            raise UserError("Existen datos duplicados para el DNI : " + self.DNI)
        else :
            self.student_id=student_id


        partner_id = self.student_id.partner_id

        student = self.student_id

        if self.invoice_id:
            raise UserError("Ya existe un documento vinculado para el alumno con el DNI : "+ self.DNI)

        self.tax = tax

        lineas = []

        if self.type == 'pension':
            self.product1 = self.env['product.product'].search([('default_code', '=', aniomes(self.date_due))])
            self.product2 = mora
            account1_id = self.product1.property_account_income_id.id
            account2_id = self.product2.property_account_income_id.id

            account_id=False

            if not self.product1:
                raise UserError("No se encuentra el producto para la persona : " + self.DNI)

            if self.product1.property_account_income_id:
                account_id = self.product1.property_account_income_id.id
            if not account_id:
                account_id = self.product1.categ_id.property_account_income_categ_id.id
            if not account_id:
                raise UserError(
                   _('There is no income account defined for this product: "%s". \
                       You may have to install a chart of account from Accounting \
                       app, settings menu.') % (self.product1.name))

            if self.mora == 0:
                lineas = [(0,_, {
                    'name': self.product1.name,
                    'origin': self.n_operacion,
                    'account_id': account_id,
                    'price_unit': self.amount,
                    'quantity': 1.0,
                    'discount': 0.0,
                    'uom_id': self.product1.uom_id.id,
                    'product_id': self.product1.id,
                    'invoice_line_tax_ids': [(6, _, [self.tax.id])]
                })]
            else :
                lineas = [(0,_, {
                    'name': self.product1.name,
                    'origin': self.n_operacion,
                    'account_id': account_id,
                    'price_unit': self.amount,
                    'quantity': 1.0,
                    'discount': 0.0,
                    'uom_id': self.product1.uom_id.id,
                    'product_id': self.product1.id,
                    'invoice_line_tax_ids': [(6, _, [self.tax.id])]
                }), (0,_, {
                    'name': self.product2.name,
                    'origin': self.n_operacion,
                    'account_id': account_id,
                    'price_unit': self.mora,
                    'quantity': 1.0,
                    'discount': 0.0,
                    'uom_id': self.product2.uom_id.id,
                    'product_id': self.product2.id,
                    'invoice_line_tax_ids': [(6, _, [self.tax.id])]
                })]

        elif self.type == 'matricula':

            self.product1 = self.env['product.product'].search([('default_code', '=', aniomesmatricula(self.date_due))])
            account1_id = self.product1.property_account_income_id.id

            account_id = False

            if not self.product1:
                raise UserError("No se encuentra el producto para la persona : " + self.DNI)

            if self.product1.property_account_income_id:
                account_id = self.product1.property_account_income_id.id
            if not account_id:
                account_id = self.product1.categ_id.property_account_income_categ_id.id
            if not account_id:
                raise UserError(
                    _('There is no income account defined for this product: "%s". \
                                   You may have to install a chart of account from Accounting \
                                   app, settings menu.') % (self.product1.name))

            lineas = [(0,_, {
                'name': self.product1.name,
                'origin': self.n_operacion,
                'account_id': account_id,
                'price_unit': self.amount,
                'quantity': 1.0,
                'discount': 0.0,
                'uom_id': self.product1.uom_id.id,
                'product_id': self.product1.id,
                'invoice_line_tax_ids': [(6, _, [self.tax.id])]
            })]



        if self.amount <= 0.00:
            raise UserError(_('The value of the deposit amount must be \
                             positive.'))

        invoice = inv_obj.create({
            'journal_id' : 12,
            'name': student.name,
            'origin': self.n_operacion or False,
            'type': 'out_invoice',
            'reference': self.reference or False,
            'account_id': partner_id.property_account_receivable_id.id,
            'partner_id': partner_id.id,
            'date_invoice' : self.date_pay,
            'date_due' : self.date_due,
            'invoice_line_ids': lineas,
        })

        invoice.compute_taxes()
        #self.state = 'invoice'
        self.invoice_id = invoice.id
        return True

    def action_get_invoice(self):
        value = True
        if self.invoice_id:
            form_view = self.env.ref('account.invoice_form')
            tree_view = self.env.ref('account.invoice_tree')
            value = {
                'domain': str([('id', '=', self.invoice_id.id)]),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.invoice',
                'view_id': False,
                'views': [(form_view and form_view.id or False, 'form'),
                          (tree_view and tree_view.id or False, 'tree')],
                'type': 'ir.actions.act_window',
                'res_id': self.invoice_id.id,
                'target': 'current',
                'nodestroy': True
            }
        return value

    @api.multi
    def invoice_all(self, values):
        tax = self.env['account.tax'].search([('name', '=', 'INAFECTO')])
        mora = self.env['product.product'].search([('name', '=', 'MORA')])
        # raise UserError(values)
        for record in values:
            record.get_invoice(tax,mora)


class OpStudent(models.Model):
    _inherit = 'op.student'

    fees_detail_ids = fields.One2many('op.student.fees.details', 'student_id',
                                      'Fees Collection Details')


def aniomes(fecha):
    if fecha:
        fecha = str(fecha)
        return fecha[0:7]
    else:
        return ""

def aniomesmatricula(fecha):
    if fecha:
        fecha = str(fecha)
        return fecha[0:5]+"00"
    else:
        return ""