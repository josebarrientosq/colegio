# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api


class WizardOpStudent(models.TransientModel):
    _name = 'wizard.op.fees'
    _description = "Crear invoice desde pagos"

    def _get_fees(self):
        if self.env.context and self.env.context.get('active_ids'):
            return self.env.context.get('active_ids')
        return []

    fees_ids = fields.Many2many('op.student.fees.details', default=_get_fees, string='Pagos')

    @api.multi
    def create_fees_invoice(self):
        #user_group = self.env.ref('openeducat_core.group_op_student')
        active_ids = self.env.context.get('active_ids', []) or []
        records = self.env['op.student.fees.details'].browse(active_ids)
        self.env['op.student.fees.details'].invoice_all(records)
