# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from openerp import models, fields, api
from odoo.exceptions import ValidationError, UserError


class WizardOpStudent(models.TransientModel):
    _name = 'wizard.op.admission'
    _description = "Create Student for selected Admission(s)"

    def _get_admission(self):
        if self.env.context and self.env.context.get('active_ids'):
            return self.env.context.get('active_ids')
        return []

    admission_ids = fields.Many2many(
        'op.admission', default=_get_admission, string='Admissions')

    @api.multi
    def create_student_admission(self):
        #user_group = self.env.ref('openeducat_core.group_op_student')
        active_ids = self.env.context.get('active_ids')
        records = self.env['op.admission'].browse(active_ids)
        #raise UserError(records)
        self.env['op.admission'].enroll_all(records)
