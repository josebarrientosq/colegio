# -*- coding: utf-8 -*-
{
    'name': "POS Journal Sequence",

    'summary': """
        Point Of Sale, Journal""",

    'description': """
        Agregra Diarios y secuencias al punto de venta
    """,

    'author': "knd",
    'website': "https://www.knd.com",

    
    "category": "Point Of Sale",
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'point_of_sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/assets.xml',
        'views/pos_config_view.xml',
        'views/pos_journal_sequence_template.xml',
    ],
    "qweb": [
        'static/src/xml/pos.xml',
    ],
}