from odoo import api,models,fields


class AccountLogStatus(models.Model):
    _name = "account.log.status"
    _order = "create_date desc"

    name = fields.Char("Nombre")
    api_user_id = fields.Char("API User id")
    content_xml = fields.Text("Respuesta de SUNAT base64 ZIP")
    description = fields.Char("Descripción")
    response_xml = fields.Text("Respuesta de SUNAT")
    request_json = fields.Text("Contenido de Comprobante en JSON")
    signed_xml = fields.Text("Envío de comprobante ZIP en base64")
    response_json = fields.Text("Respuesta del API")
    unsigned_xml = fields.Text("Comprobante sin Firmar")
    signed_xml_data = fields.Text("Comprobante XML Firmado")
    status = fields.Text("Estado de envío a SUNAT")
    date_request = fields.Date("Fecha de Envío al API")
    date_issue = fields.Date("Fecha de Emisión a SUNAT")
    api_request_id = fields.Char("Identificador de Envío")
    digest_value = fields.Char("Digest Value")

    account_invoice_id = fields.Many2one("account.invoice",string="Comprobante")
    account_summary_id = fields.Many2one("account.summary",string="Resumen Diario")
    guia_remision_id = fields.Many2one("efact.guia_remision",string="Guía de Remisión")
    
