from odoo import fields,api,models 
from odoo.exceptions import UserError, ValidationError

import re

patron_dni = re.compile("\d{8}$")
patron_ruc = re.compile("[12]\d{10}$")

class ResPartner(models.Model):
    _inherit = "res.partner"
    
    
    @api.onchange('tipo_documento')
    def _onchange_tipo_documento(self):
        for record in self:
            if record.tipo_documento == '6':
                record.company_type = "company"
            else:
                record.company_type = "person"
    
    
    @api.onchange('company_type')
    def _onchange_company_type(self):
        for record in self:
            if record.company_type == "company":
                self.tipo_documento = '6'
            else:
                if not self.tipo_documento:
                    self.tipo_documento = '-'
                if not self.vat:
                    self.vat = '0'

    @api.onchange('name')
    def _onchange_name(self):
        for record in self:
            if record.name:
                self.name = self.name.strip()
                self.name = self.name.replace("\n"," - ")
    
    
    @api.constrains('name')
    def _check_name(self):
        for record in self:
            if record.name and record.type not in ["delivery","other"]:
                if (len(record.name)<4 or  len(record.name) >250) :
                    raise UserError("La cantidad de carácteres del cliente debe ser mayor a 4 y menor a 250.") 
            

    @api.model
    def create(self, vals):
        vat = vals.get("vat",False)
        vat = vat.strip() if vat else ""
        tipo_documento = vals.get("tipo_documento",False)
        name = vals.get("name",False)
        name = name.strip() if name else ""
            
        cliente_varios = self.env["res.partner"].search([("tipo_documento","=","-"),("name","=",name)])
        if cliente_varios.exists():
            msg = "El cliente ya existe con el nombre {}.".format(cliente_varios[0].name)
            raise UserError(msg)

        if vals.get("vat",False):
            cliente = self.env["res.partner"].search([("vat","=",vat),("tipo_documento","=",tipo_documento),("company_id","=",self.env.user.company_id.id)])
            if cliente.exists():
                msg = "El Número de Identidad del cliente ingresado ya existe y se encuentra con el nombre {}.".format(cliente[0].name)
                raise UserError(msg)
                
        if  vals.get("tipo_documento", False) =="6":
            vals.update({"company_type":"company"})
        else:
            vals.update({"company_type":"person"})
        
        if not vals.get("registration_name",False):
            vals.update({"registration_name":vals.get("name")})
        
        if not vals.get("zip",False):
            vals.update({"zip":"-"})

        return super(ResPartner, self).create(vals)