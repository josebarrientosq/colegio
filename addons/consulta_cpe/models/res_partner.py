from odoo import fields, models, api

class ResPartner(models.Model):
    _inherit = 'res.partner'

    password = fields.Char('password', default='')

    def get_clave(self):
        self.password = ''