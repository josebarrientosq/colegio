# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug.utils
import os

class consulta_cpe(http.Controller):

    @http.route('/consulta', auth='public')
    def login(self, **kwargs):
        return request.render('consulta_cpe.login')

    @http.route('/autenticando', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def autenticar(self, **post):
        dni = post.get("dni")
        password = post.get("password")

        cliente= request.env["res.partner"].sudo().search([('vat','=',dni)])


        if cliente:
            nombre = cliente.name
            os.system("echo '%s'" % (cliente.password))
            if cliente.password == password or cliente.password =='' or cliente.password is False:
                dni = cliente.vat
                return http.request.render('consulta_cpe.form',{'name': nombre,'dni':dni})
            else:
                return 'clave'
        else:
            return 'dni'

    @http.route('/busqueda', type='http', auth='public', method=["POST"], csrf=False)
    def consulta(self, **post):
        dni = post.get("dni")
        cliente= request.env["res.partner"].sudo().search([('vat','=',dni)], limit=1)
        documento = request.env["account.invoice"].sudo().search([('partner_id','=',cliente.id),('state','=','open')])

        return request.render('consulta_cpe.documentos',{'documento' : documento})


    @http.route("/consulta/comprobante/pdf/<identificador>", auth='public', type='http', method=["GET"], csrf=False)
    def get_pdf(self, identificador):

        invoice = request.env["account.invoice"].sudo().search([('number',"=",identificador)])

        if invoice:
            pdf = request.env.ref('report_fact.custom_account_invoices_ticket').sudo().render_qweb_pdf([invoice.id])[0]
            pdfhttpheaders = [('Content-Type', 'application/pdf'),
                              ('Content-Length', len(pdf)),
                              ('Content-Disposition', 'attachment; filename="%s.pdf"' % (invoice.number))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect('/')


    @http.route("/consulta/comprobante/xml/<identificador>", type='http', method=["GET"],csrf=False)
    def comprobante_xml(self,identificador):
        log = request.env["account.log.status"].search([["api_request_id","=",identificador]])
        xml = log.signed_xml_data
        xml_headers = [
            ('Content-Type', 'application/xml'),
            ('Content-Length', len(xml)),
            ('Content-Disposition', 'attachment; filename="%s.xml"'%(log.name)),
        ]
        return request.make_response(xml, headers=xml_headers)
