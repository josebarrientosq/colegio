from odoo import models, fields, api, _


class marcas(models.Model):
    _name = 'marca'
    _description = 'Marcaciones del biometrico'
    _order = 'timestamp DESC, user_id, status, device_id'

    device_id = fields.Many2one('attendance.device', string='Attendance Device', required=True, ondelete='restrict', index=True)
    user_id = fields.Many2one('attendance.device.user', string='Device User', required=True, ondelete='cascade', index=True)
    timestamp = fields.Datetime(string='Timestamp', required=True, index=True)
    status = fields.Integer(string='Device Attendance State', required=True,
                            help='The state which is the unique number stored in the device to'
                            ' indicate type of attendance (e.g. 0: Checkin, 1: Checkout, etc)')
    attendance_state_id = fields.Many2one('attendance.state', string='Odoo Attendance State',
                                          help='This technical field is to map the attendance'
                                               ' status stored in the device and the attendance status in Odoo',
                                          required=True, index=True)

    activity_id = fields.Many2one('attendance.activity', related='attendance_state_id.activity_id', store=True,
                                  index=True)

    type = fields.Selection([('checkin', 'Check-in'),
                             ('checkout', 'Check-out')], string='Activity Type', related='attendance_state_id.type',store=True)
    employee_id = fields.Many2one('hr.employee', string='Empleado', related='user_id.employee_id', store=True,
                                  index=True)
