from odoo import models, fields, api, SUPERUSER_ID, sql_db, _, registry
from odoo.tools.safe_eval import test_python_expr
from odoo.exceptions import ValidationError, UserError

class Test(models.Model):
    _name = 'hr.asistencia.test'
    _description = 'asistencia remote'

    name = fields.Char('database')

    def test(self):
        self.ensure_one()
        db = sql_db.db_connect(self.name)
        with api.Environment.manage(), db.cursor() as cr:
            env = api.Environment(cr, SUPERUSER_ID, {})
            module_ids = env['ir.module.module'].search([('state', '=', 'uninstalled')])
            module_ids.button_immediate_install()