# -*- coding: utf-8 -*-

import datetime
from datetime import datetime,date
from pytz import timezone
from odoo import models, fields, api, exceptions,_
from odoo.exceptions import UserError,ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import logging,os
_logger = logging.getLogger(__name__)

def totalminutos(datetime):
    a = datetime.time()
    f = round(a.hour * 60 + a.minute) -300
    return f

def totalhoras(datetime):
    a = datetime.time()
    return round(a.hour)

def difpositivo(num):
    if(num<0):
        return 0
    else:
        return num

""""
class HrAttendance(models.Model):
    _inherit = ['hr.attendance']

    tardanza = fields.Float(string='tardanza')
    reclamo = fields.Text("Reclamo")
    anexo = fields.Binary("Anexo", attachment=True)



    #@api.onchange('check_in')
    def _compute_tardanza(self):
        calendario = self.employee_id.resource_calendar_id
        if (self.check_in):
            posicion_dia = datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S").weekday()
            start_dt = datetime.strptime(self.check_in, "%Y-%m-%d %H:%M:%S")  # convert into datetime fromat


            if ( totalminutos(start_dt)>calendario.cambio_turno*60):  #si supera el cambio de turno (en minutos) devuelve 1 posicion mas
                posicion_calendario = 2*posicion_dia+1
            else:
                posicion_calendario = 2*posicion_dia

            entrada1turno = calendario.attendance_ids[posicion_calendario].hour_from

            difentrada1=difpositivo(totalminutos(start_dt)- entrada1turno*60)

            if difentrada1 < calendario.max_tardanza*60 :
                self.tardanza = difentrada1
            else :
                self.tardanza = calendario.max_tardanza*60


    def asignar_falta(self):
        falta  = self.env['hr.holidays']
        tipo_falta = self.env['hr.holidays.status'].search([('name',"=",'FALTA')])
        today = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        empleados = self.env['hr.employee'].search([('contract_id.state',"=",'open')]) #contratos vigentes
        for emp in empleados:
            falta.create({
                'name': 'FALTA POR INASISTENCIA',
                'holiday_type': 'employee',
                'holiday_status_id': tipo_falta.id,
                'date_from': today,
                'date_to': today,
                'report_notes': 'Falta',
                'number_of_days_temp': 1,
                'employee_id': emp.id
            })
            #asistencia = self.env['hr.attendance'].search([('check_in','>=',today) ,('employee_id', '=', emp.id)])

"""



class ResourceCalendar(models.Model):
    _inherit = "resource.calendar"

    antes_entrar = fields.Float('Antes de entrar' , default=0)
    despues_entrar = fields.Float('Despues de entrar', default=0)
    antes_salir = fields.Float('Antes de salir',default=0)
    despues_salir = fields.Float('Despues de salir',default=0)

    horario_oficina = fields.Boolean('Horario oficina')
    sin_tardanza = fields.Boolean('sin tardanza')
    sin_falta = fields.Boolean('sin falta')


class ResourceCalendarAttendance(models.Model):
    _inherit = "resource.calendar.attendance"

    activity_id = fields.Many2one('attendance.activity', string='Attendance Activity',
                                  help='This field is to group attendance into multiple Activity (e.g. Overtime, Normal Working, etc)')


