from odoo import models, fields , _
import datetime
from datetime import datetime,date
import pytz
from odoo.exceptions import UserError, ValidationError
import os


class Hrattendance(models.Model):
    _inherit = ['hr.attendance']

    calendario_id = fields.Many2one('resource.calendar', related='employee_id.contract_id.resource_calendar_id')

    calendar_attendance = fields.Many2one('resource.calendar.attendance', compute='compute_attendance')
    entrada_calendario = fields.Float("Entrada Horario", compute='get_entrada_cal')
    salida_calendario = fields.Float("Salida Horario", compute='get_salida_cal')


    activity_id = fields.Many2one('attendance.activity', index=True)

    entrada_hour = fields.Float('Hora entrada', compute='get_entrada_hour')
    salida_hour = fields.Float('Hora salida', compute='get_salida_hour')

    tardanza = fields.Float(string='Tardanza', compute='compute_tardanza2')

    reclamo = fields.Text("Reclamo")
    anexo = fields.Binary("Anexo", attachment=True)

    def compute_attendance(self):
        for record in self:
            if record.check_in and record.activity_id:
                fecha = datetime.strptime(str(record.check_in), "%Y-%m-%d %H:%M:%S")
                posicion_dia = fecha.weekday()
                calendar_attendance = self.env['resource.calendar.attendance'].search(
                [('calendar_id', '=', record.calendario_id.id),
                 ('dayofweek', '=', posicion_dia),
                 ('activity_id', '=', record.activity_id.id)],limit=1)

                record.calendar_attendance = calendar_attendance

    def get_entrada_cal(self):
        for record in self:
            if record.calendar_attendance:
                record.entrada_calendario = record.calendar_attendance.hour_from

    def get_salida_cal(self):
        for record in self:
            if record.calendar_attendance:
                record.salida_calendario = record.calendar_attendance.hour_to


    def get_entrada_hour(self):
        for record in self:
            if record.check_in:
                fecha_local = self.convert_utc_time_to_tz(record.check_in)
                start_dt = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
                tiempo = start_dt.time()
                record.entrada_hour = tiempo.hour + tiempo.minute/60

    def get_salida_hour(self):
        for record in self:
            if record.check_out:
                fecha_local = self.convert_utc_time_to_tz(record.check_out)
                start_dt = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
                tiempo = start_dt.time()
                record.salida_hour = tiempo.hour + tiempo.minute/60

    def compute_tardanza2(self):
        for record in self:
            if record.check_in:
                record.tardanza = (record.entrada_hour - record.entrada_calendario)*60

    def fecha_a_hora_str(self,fecha):
        fecha_local = self.convert_utc_time_to_tz(fecha)
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        return fecha_obj.strftime("%H:%M")

    def convert_utc_time_to_tz(self, utc_datetime_str, tz_name=None):
        utc_time = fields.Datetime.from_string(utc_datetime_str)
        tz_name = tz_name or self._context.get('tz') or self.env.user.tz
        if not tz_name:
            raise ValidationError(_("Local time zone is not defined. You may need to set a time zone in your user's Preferences."))
        tz = pytz.timezone(tz_name)
        tz_time = pytz.utc.localize(utc_time, is_dst=None).astimezone(tz)
        return fields.Datetime.to_string(tz_time)