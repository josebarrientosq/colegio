from odoo import models, fields, api, _
import datetime
from datetime import datetime,date
import os
from odoo.tools.profiler import profile

class asistencia_mtpe(models.Model):
    _name = 'asistencia.mtpe'
    _description = 'asistencia mtpe'
    _inherit = ['to.base']


    employee_id = fields.Many2one('hr.employee', string='Empleado', required=True, index=True)
    calendario_id = fields.Many2one('resource.calendar', related='employee_id.contract_id.resource_calendar_id')
    #calendar_name = fields.Char(related='employee_id.contract_id.resource_calendar_id.name')
    calendar_attendance = fields.Many2one('resource.calendar.attendance', compute = 'compute_attendance')
    entrada_calendario = fields.Float("Entrada Horario", compute='get_entrada_cal')
    salida_calendario = fields.Float("Salida Horario" , compute='get_salida_cal')


    fecha = fields.Datetime(string='Fecha calculo', required=True, index=True)
    dia = fields.Char('Dia',compute = 'get_dia')
    activity_id = fields.Many2one('attendance.activity',"Turno",index=True)


    entrada = fields.Datetime(string='Entrada')
    salida = fields.Datetime(string='Salida')

    entrada_hour = fields.Float('Hora entrada', compute='get_entrada_hour')
    salida_hour = fields.Float('Hora salida', compute='get_salida_hour')

    marcas = fields.Char('Todas las marcas', default="")

    tardanza = fields.Float(string='Tardanza', compute='compute_tardanza4', store="True")

    hora_efectiva = fields.Float(string='H.EFEC', compute='compute_hora_efectiva',store="True" )
    hora_permanencia = fields.Float(string='H.PERM', compute='compute_hora_permanencia' , store="True")

    asistencia = fields.Many2one('hr.attendance', string='Asistencia', ondelete='set null',
                                       help='The technical field to link Device Attendance Data with Odoo Attendance Data',
                                       index=True)

    falta = fields.Boolean('Falta', compute = 'set_falta')

    inasistencia = fields.Many2one('hr.holidays', 'Inasistencia')
    estado_inasistencia = fields.Selection([
        ('draft', 'Borrador'),
        ('cancel', 'Cancelado'),
        ('confirm', 'Confirmado'),
        ('refuse', 'Rechazado'),
        ('validate1', 'Segunda aprobacion'),
        ('validate', 'Aprobado')
        ], string='Estado', compute='compute_estado')

    observacion = fields.Char('Observacion', compute='compute_tardanza4', store="True")
    motivo = fields.Char('Motivo')

    def compute_estado(self):
        for record in self:
            if record.inasistencia:
                record.estado_inasistencia = record.inasistencia.state

    def compute_attendance(self):
        for record in self:
            if record.fecha:
                fecha = datetime.strptime(str(record.fecha), "%Y-%m-%d %H:%M:%S")
                posicion_dia = fecha.weekday()
                calendar_attendance = self.env['resource.calendar.attendance'].search(
                [('calendar_id', '=', record.calendario_id.id),
                 ('dayofweek', '=', posicion_dia),
                 ('activity_id', '=', record.activity_id.id)],limit=1)

                record.calendar_attendance = calendar_attendance

    @profile
    def _compute_tardanza(self):
        calendar_attendance=""
        if self.fecha:
            fecha = datetime.strptime(str(self.fecha), "%Y-%m-%d %H:%M:%S")
            posicion_dia = fecha.weekday()
            calendar_attendance = self.env['resource.calendar.attendance'].search(
                [('calendar_id', '=', self.calendario_id.id),
                 ('dayofweek', '=', posicion_dia),
                 ('activity_id', '=', self.activity_id.id)])
        if calendar_attendance:
            os.system("echo '%s'" % ("encontro horario"))
            hora_entrada = calendar_attendance.hour_from

        if (self.entrada):
            fecha_local = self.fecha_local(self.entrada)
            start_dt = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
            tiempo = start_dt.time()
            minutos = round(tiempo.hour * 60 + tiempo.minute)
            self.tardanza = minutos - hora_entrada * 60

    @api.depends('entrada' , 'salida')
    def compute_tardanza2(self):
        for record in self:
            if record.entrada:
                if (record.entrada_hour > record.entrada_calendario):
                    record.tardanza_real = (record.entrada_hour - record.entrada_calendario)*60
                else :
                    record.tardanza_real= 0

    def compute_tardanza3(self):
        for record in self:
            if record.entrada:
                if (record.entrada_hour - record.entrada_calendario > 10):
                    record.tardanza = (record.entrada_hour - record.entrada_calendario)*60
                else :
                    record.tardanza = 0

    @api.depends('entrada' , 'salida')
    def compute_tardanza4(self):
        for record in self:
            if record.entrada:
                if (record.entrada_hour > record.entrada_calendario):
                    record.tardanza = (record.entrada_hour - record.entrada_calendario)*60
                else :
                    record.tardanza = 0
            else:
                if record.salida:    # autocompletado de tardanza , olvido marcar la entrada perso si marco la salida
                    record.tardanza = 25
                    record.observacion = "Penalidad por no marcar entrada"
                else:
                    record.tardanza = 0 #es una falta , no va tardanza

            if record.entrada and record.calendario_id.horario_oficina and record.activity_id.code == 1:   # en oficina no se considera una hora de refrigerio
                asistencia_anterior = self.env['asistencia.mtpe'].browse([record.id-1])
                if asistencia_anterior.salida_hour: #turno mañana
                    if (record.entrada_hour - asistencia_anterior.salida_hour)>1:
                        record.tardanza = (record.entrada_hour - asistencia_anterior.salida_hour-1)*60
                        record.observacion = "Refrigerio mayor 1 hora"
                    else:
                        record.tardanza = 0
                else:
                    record.tardanza = 25  #penalidad por no marcar salida
                    record.observacion = "Penalidad por no marcar salida anterior"

            if record.calendario_id.sin_tardanza:   # sin descuento por tardanza
                record.tardanza = 0

    def get_dia(self):
        for record in self:
            if record.fecha:
                fecha = datetime.strptime(record.fecha, "%Y-%m-%d %H:%M:%S")
                record.dia = fecha.weekday()

    def get_entrada_cal(self):
        for record in self:
            if record.fecha:
                record.entrada_calendario = record.calendar_attendance.hour_from

    def get_salida_cal(self):
        for record in self:
            if record.fecha:
                record.salida_calendario = record.calendar_attendance.hour_to






    def get_entrada_hour(self):
        for record in self:
            if record.entrada:
                fecha_local = self.fecha_local(record.entrada)
                start_dt = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
                tiempo = start_dt.time()
                record.entrada_hour = tiempo.hour + tiempo.minute/60

    def get_salida_hour(self):
        for record in self:
            if record.salida:
                fecha_local = self.fecha_local(record.salida)
                start_dt = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
                tiempo = start_dt.time()
                record.salida_hour = tiempo.hour + tiempo.minute/60


    def set_falta(self):
        for record in self:
            if record.entrada_hour == 0 and record.salida_hour == 0:
                record.falta = True
            else:
                record.falta = False

            if record.calendario_id.sin_falta :   # sin falta
                record.falta = False

    @api.depends('tardanza')
    def compute_hora_efectiva(self):
        for record in self:
            if record.salida_calendario and record.entrada_calendario:
                record.hora_efectiva = record.salida_calendario-record.entrada_calendario-record.tardanza/60
            else:
                record.hora_efectiva=0

    @api.depends('entrada_hour','salida_hour')
    def compute_hora_permanencia(self):
        for record in self:
            if record.entrada_hour and record.salida_hour:
                record.hora_permanencia = record.salida_hour - record.entrada_hour
            else:
                record.hora_permanencia = 0


    def crear_asistencia(self):
        hr_attendance = self.env['hr.attendance']
        for record in self:

            if not record.falta:
                record.asistencia = hr_attendance.create({
                    'employee_id' : record.employee_id.id,
                    'activity_id' : record.activity_id.id,
                    'check_in' : record.entrada,
                    'check_out': record.salida,
                 })

    def crear_inasistencia(self):
        falta = self.env['hr.holidays']
        tipo_falta = self.env['hr.holidays.status'].search([('codigo', "=", '07')])
        for record in self:
            os.system("echo '%s'" % ("no hay asistencia"))
            if record.activity_id.code == 0 and record.falta:
                os.system("echo '%s'" % ("crear falta"))
                os.system("echo '%s'" % (record.fecha))
                record.inasistencia = falta.create({
                    'name': 'falta no justificada',
                    'holiday_type': 'employee',
                    'holiday_status_id': tipo_falta.id,
                    'date_from': record.fecha,
                    'date_to': record.fecha,
                    'report_note': 'Falta',
                    'number_of_days_temp': 1,
                    'employee_id': record.employee_id.id
                })


    def fecha_local(self,fecha):
        return self.convert_utc_time_to_tz(fecha,'America/Lima')


    def fecha_a_hora_str(self,fecha):
        fecha_local = self.convert_utc_time_to_tz(fecha,'America/Lima')
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        return fecha_obj.strftime("%H:%M")


    @api.multi
    def _get_attachment_count(self):
        for order in self:
            attachment_ids = self.env['ir.attachment'].search([('asistencia_attachment_id', '=', order.id)])
            order.attachment_count = len(attachment_ids)


    attachment_count = fields.Integer('Attachments', compute='_get_attachment_count')


    @api.multi
    def attachment_on_asistencia_button(self):
        self.ensure_one()
        return {
            'name': 'Attachment.Details',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,tree,form',
            'res_model': 'ir.attachment',
            'domain': [('asistencia_attachment_id', '=', self.id)],

        }


class ir_attachment(models.Model):
    _inherit='ir.attachment'

    asistencia_attachment_id  =  fields.Many2one('asistencia.mtpe', 'asistencia archivo')