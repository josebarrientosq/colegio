from odoo import models, fields, api, SUPERUSER_ID, sql_db, _, registry
from odoo.exceptions import ValidationError, UserError
import datetime
from datetime import datetime, date, timedelta , time
import os


class AttendanceWizard(models.TransientModel):
    _name = 'attendance.wizard.mtpe'
    _description = 'Attendance Wizard'
    _inherit = ['to.base']

    fecha_calculo = fields.Datetime('Fecha calculo')
    fecha_inicio = fields.Date('Fecha inicio' , required=True)
    fecha_fin = fields.Date('Fecha fin' , required=True)
    basedatos = fields.Char('database', required=True)

    empleados_ids = fields.Many2many('hr.employee', string='Empleados', required=True)


    def test(self):
        self.ensure_one()
        db = sql_db.db_connect(self.basedatos)
        with api.Environment.manage(), db.cursor() as cr:
            env = api.Environment(cr, SUPERUSER_ID, {})
            os.system("echo '%s'" % ('testeando company ------------------'))
            company = env['res.company'].search([('tipo_envio','=',2)])
            os.system("echo '%s'" % (company.name))


    def corregir_turno(self):
        user_attendance_ids = self.env['user.attendance']

        user_attendances = user_attendance_ids.search(
            [('timestamp', '>', self.fecha_inicio),
             ('timestamp', '<', self.fecha_fin),
             ], order='timestamp')

        for marca in user_attendances:
            os.system("echo '%s'" % (marca))
            fecha_local = self.convert_utc_time_to_tz(marca.timestamp)
            start_dt = datetime.strptime(str(fecha_local), "%Y-%m-%d %H:%M:%S")
            tiempo = start_dt.time()
            hora= tiempo.hour + tiempo.minute / 60

            if hora >17:
                marca.attendance_state_id=self.env['user.attendance'].browse([3]).id

    def corregir_turno2(self):
        user_attendance_ids = self.env['marca']

        user_attendances = user_attendance_ids.search(
            [('timestamp', '>', self.fecha_inicio),
             ('timestamp', '<', self.fecha_fin),
             ], order='timestamp')
        for marca in user_attendances:
            os.system("echo '%s'" % (marca))
            fecha_local = self.convert_utc_time_to_tz(marca.timestamp)
            start_dt = datetime.strptime(str(fecha_local), "%Y-%m-%d %H:%M:%S")
            tiempo = start_dt.time()
            hora= tiempo.hour + tiempo.minute / 60

            if hora >17:
                marca.status=3
            elif hora>15:
                marca.status = 2
            elif hora > 13:
                marca.status = 1
            else :
                marca.status = 0

    @api.multi
    def generar(self):

        employee_ids = self.env['hr.employee'].search([('contract_id.state', "=", 'draft')])  # contratos vigentes
        os.system("echo '%s'" % (employee_ids))
        activity_ids = self.env['attendance.activity'].search([])
        user_attendance_ids = self.env['user.attendance']
        # calendar = self.env['resource.calendar']
        asistencia_mtpe = self.env['asistencia.mtpe']
        os.system("echo '%s'" % ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))
        os.system("echo '%s'" % (str(self.fecha_calculo)))
        fecha = datetime.strptime(str(self.fecha_calculo), "%Y-%m-%d %H:%M:%S")
        os.system("echo '%s'" % (fecha))

        fecha_local = self.convert_utc_time_to_tz(str(self.fecha_calculo))
        os.system("echo '%s'" % (fecha_local))
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        os.system("echo '%s'" % (fecha_obj))
        timestampinicio= fecha_obj.replace(hour=0, minute=0, second=0, microsecond=0)
        os.system("echo '%s'" % (timestampinicio))
        timestamp_inicio_str = timestampinicio.strftime("%Y-%m-%d %H:%M:%S")
        os.system("echo '%s'" % (timestamp_inicio_str))
        timestampfin = fecha.replace(hour=23, minute=59, second=59, microsecond=0)
        timestamp_fin_str = timestampfin.strftime("%Y-%m-%d %H:%M:%S")


        fecha_calculo = self.convert_utc_time_to_tz(timestamp_inicio_str)
        os.system("echo '%s'" % (timestamp_inicio_str))
        os.system("echo '%s'" % (timestamp_fin_str))

        posicion_dia = fecha.weekday()

        for employee in employee_ids:

            if employee.contract_id.state == 'open':
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))


                for activity_id in activity_ids:
                    os.system("echo '%s'" % (activity_id.name))
                    calendar = employee.contract_id.resource_calendar_id
                    calendar_attendance = self.env['resource.calendar.attendance'].search(
                        [('calendar_id', '=', calendar.id),
                         ('dayofweek', '=', posicion_dia),
                         ('activity_id', '=', activity_id.id)])

                    if calendar_attendance:
                        entrada_calendario = str(calendar_attendance.hour_from)
                        salida_calendario = str(calendar_attendance.hour_to)
                        os.system("echo '%s'" % (str(calendar_attendance.hour_from)))
                        os.system("echo '%s'" % (str(calendar_attendance.hour_to)))
                        user_attendances = user_attendance_ids.search(
                                     [('employee_id', '=', employee.id),
                                     ('activity_id', '=', activity_id.id),
                                     ('timestamp', '>', timestamp_inicio_str),
                                     ('timestamp', '<', timestamp_fin_str),
                                     ], order='timestamp')

                        entrada = ""
                        salida = ""

                        marcas = [(4, line.id) for line in user_attendances]


                        checkin = []
                        checkout = []
                        entrada_str = ""
                        salida_str = ""
                        todaslasmarcas=""
                        for marca in user_attendances:
                            todaslasmarcas += self.fecha_a_hora_str(marca.timestamp)+" "

                            if marca.type == 'checkin':
                                checkin.append(marca)
                            if marca.type == 'checkout':
                                checkout.append(marca)

                        num_checkin = len(checkin)
                        num_checkout = len(checkout)

                        if num_checkin>0:
                            entrada = checkin[0].timestamp

                        else:
                            entrada = calendar_attendance.hour_from + 0.33
                            #se agrega 20 minutos si no hay marca de entrada

                        entrada_str = self.fecha_a_hora_str(entrada)

                        if num_checkout>0:
                            salida = checkout[num_checkout-1].timestamp
                        else:
                            salida = calendar_attendance.hour_to

                        salida_str = self.fecha_a_hora_str(salida)



                        os.system("echo '%s'" % (".........."))
                        os.system("echo '%s'" % (entrada))
                        os.system("echo '%s'" % (entrada_str))
                        os.system("echo '%s'" % (salida))
                        os.system("echo '%s'" % (salida_str))
                        asistencia_mtpe.create({
                            'fecha': fecha_calculo,
                            'employee_id': employee.id,
                            'activity_id': activity_id.id,

                            'user_attendance_ids': marcas,
                            'entrada': entrada,
                            'salida': salida,

                        })

                    else:
                        os.system("echo '%s'" % ("no tiene horario ese dia"))

    def generar_data_test(self):
        fecha_fin = datetime.strptime(str(self.fecha_fin), "%Y-%m-%d")
        fecha_inicio = datetime.strptime(str(self.fecha_inicio), "%Y-%m-%d")

        for n in range(int((fecha_fin - fecha_inicio).days)):
            os.system("echo '%s'" % (fecha_inicio + timedelta(n)))





    def generar_data(self):
        fecha_fin = datetime.strptime(str(self.fecha_fin), "%Y-%m-%d")
        fecha_inicio = datetime.strptime(str(self.fecha_inicio), "%Y-%m-%d")

        calendario_feriado = self.env['hr.holidays.public'].search([('year','=', 2019)])

        for n in range(int((fecha_fin - fecha_inicio).days)):
            fecha_buscar = fecha_inicio + timedelta(n)
            dia_feriado = calendario_feriado.is_public_holiday(fecha_buscar)

            if not dia_feriado:
                self.generar4(fecha_buscar)

            else:
                os.system("echo '%s'" % ("FERIADO"))



    def conectar_db(self):
        self.ensure_one()
        db = sql_db.db_connect(self.basedatos)
        with api.Environment.manage(), db.cursor() as cr:
            env = api.Environment(cr, SUPERUSER_ID, {})
            os.system("echo '%s'" % ('testeando company ------------------'))
            company = env['res.company'].search([('tipo_envio','=',2)])
            os.system("echo '%s'" % (company.name))

    def generar_data5(self):
        fecha_fin = datetime.strptime(str(self.fecha_fin), "%Y-%m-%d")
        fecha_inicio = datetime.strptime(str(self.fecha_inicio), "%Y-%m-%d")

        calendario_feriado = self.env['hr.holidays.public'].search([('year','=', 2019)])

        for n in range(int((fecha_fin - fecha_inicio).days)):
            fecha_buscar = fecha_inicio + timedelta(n)
            dia_feriado = calendario_feriado.is_public_holiday(fecha_buscar)

            if not dia_feriado:
                self.generar5(fecha_buscar)

            else:
                os.system("echo '%s'" % ("FERIADO"))


    @api.multi
    def generar5(self, pub_date):

        employee_ids = self.env['hr.employee'].search([('contract_id.state', "=", 'draft')])  # contratos vigentes
        activity_ids = self.env['attendance.activity'].search([])
        user_attendance_ids = self.env['marca']
        # calendar = self.env['resource.calendar']
        asistencia_mtpe = self.env['asistencia.mtpe']

        os.system("echo '%s'" % ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))

        min_pub_date_time = datetime.combine(pub_date, time.min)
        max_pub_date_time = datetime.combine(pub_date, time.max)

        timestamp_min_str = min_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        timestamp_max_str = max_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        posicion_dia = min_pub_date_time.weekday()
        os.system("echo '%s'" % (timestamp_min_str))
        os.system("echo '%s'" % (timestamp_max_str))
        os.system("echo '%s'" % (posicion_dia))

        fecha_calculo = self.fecha_local(timestamp_min_str)
        for employee in employee_ids:

            if employee.contract_id.state == 'open':
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))

                for activity_id in activity_ids:
                    os.system("echo '%s'" % (activity_id.name))
                    calendar = employee.contract_id.resource_calendar_id
                    calendar_attendance = self.env['resource.calendar.attendance'].search(
                        [('calendar_id', '=', calendar.id),
                         ('dayofweek', '=', posicion_dia),
                         ('activity_id', '=', activity_id.id)])
                    os.system("echo '%s'" % (calendar_attendance))

                    if calendar_attendance:
                        entrada_calendario = str(calendar_attendance.hour_from)
                        salida_calendario = str(calendar_attendance.hour_to)
                        os.system("echo '%s'" % (str(calendar_attendance.hour_from)))
                        os.system("echo '%s'" % (str(calendar_attendance.hour_to)))
                        os.system("echo '%s'" % (str(activity_id.id)))

                        self.ensure_one()
                        db = sql_db.db_connect(self.basedatos)
                        with api.Environment.manage(), db.cursor() as cr:
                            envir = api.Environment(cr, SUPERUSER_ID, {})
                            os.system("echo '%s'" % ('--------conectando con db externa------------------'))

                            company = envir['res.company'].search([('tipo_envio', '=', 2)])
                            os.system("echo '%s'" % ('--------conectando con db externa compañia------------------'))
                            os.system("echo '%s'" % (company.name))

                            user_attendances = envir['marca'].search(
                                [('employee_id.name', '=', employee.name),
                                 ('activity_id.name', '=', activity_id.name),
                                 ('timestamp', '>', timestamp_min_str),
                                 ('timestamp', '<', timestamp_max_str),
                                 ], order='timestamp')


                            entrada = ""
                            salida = ""

                            marcas = [(4, line.id) for line in user_attendances]

                            os.system("echo '%s'" % (marcas))
                            checkin = []
                            checkout = []
                            entrada_str = ""
                            salida_str = ""
                            todaslasmarcas = ""
                            for marca in user_attendances:
                                todaslasmarcas += self.fecha_a_hora_str(marca.timestamp) + " "

                                if marca.type == 'checkin':
                                    checkin.append(marca)
                                if marca.type == 'checkout':
                                    checkout.append(marca)
                            os.system("echo '%s'" % (todaslasmarcas))

                            num_checkin = len(checkin)
                            num_checkout = len(checkout)

                            if num_checkin > 0:
                                entrada = checkin[0].timestamp
                                entrada_str = self.fecha_a_hora_str(entrada)
                            if num_checkout > 0:
                                salida = checkout[num_checkout - 1].timestamp
                                salida_str = self.fecha_a_hora_str(salida)

                            os.system("echo '%s'" % (timestamp_max_str))
                            os.system("echo '%s'" % (employee.id))
                            os.system("echo '%s'" % (activity_id.id))
                            os.system("echo '%s'" % (marcas))
                            os.system("echo '%s'" % (entrada))
                            os.system("echo '%s'" % (salida))

                            asistencia_mtpe = self.env['asistencia.mtpe']

                            asistencia_mtpe.create({
                                'fecha': timestamp_max_str,
                                'employee_id': employee.id,
                                'activity_id': activity_id.id,

                                'entrada': entrada,
                                'salida': salida,

                            })


                    else:
                        os.system("echo '%s'" % ("no tiene horario ese dia"))

    def generar_data6(self):
        fecha_fin = datetime.strptime(str(self.fecha_fin), "%Y-%m-%d")
        fecha_inicio = datetime.strptime(str(self.fecha_inicio), "%Y-%m-%d")

        calendario_feriado = self.env['hr.holidays.public'].search([('year','=', 2019)])

        for n in range(int((fecha_fin - fecha_inicio).days)):
            fecha_buscar = fecha_inicio + timedelta(n)
            dia_feriado = calendario_feriado.is_public_holiday(fecha_buscar)

            if not dia_feriado:
                self.generar6(fecha_buscar)

            else:
                os.system("echo '%s'" % ("FERIADO"))


    @api.multi
    def generar6(self, pub_date):

        employee_ids = self.env['hr.employee'].search([('contract_id.state', "=", 'draft')])  # contratos vigentes
        activity_ids = self.env['attendance.activity'].search([])


        os.system("echo '%s'" % ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))
        os.system("echo '%s'" % (pub_date))
        min_pub_date_time = datetime.combine(pub_date, time.min)
        max_pub_date_time = datetime.combine(pub_date, time.max)

        timestamp_min_str = min_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        timestamp_max_str = max_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        posicion_dia = min_pub_date_time.weekday()
        os.system("echo '%s'" % (timestamp_min_str))
        os.system("echo '%s'" % (timestamp_max_str))
        os.system("echo '%s'" % (posicion_dia))

        fecha_calculo = self.fecha_local(timestamp_min_str)
        for employee in employee_ids:

            if employee.contract_id.state == 'open':
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))

                asistencia_mtpe = self.env['asistencia.mtpe']
                asistencia_pasadas = asistencia_mtpe.search(
                    [('employee_id','=',employee.id),('fecha','>',timestamp_min_str ),('fecha','<',timestamp_max_str)])

                os.system("echo '%s'" % ("Asistencia pasadas para borrar"))
                os.system("echo '%s'" % (asistencia_pasadas))
                asistencia_pasadas.unlink()


                for activity_id in activity_ids:
                    os.system("echo '%s'" % (activity_id.name))
                    calendar = employee.contract_id.resource_calendar_id
                    calendar_attendance = self.env['resource.calendar.attendance'].search(
                        [('calendar_id', '=', calendar.id),
                         ('dayofweek', '=', posicion_dia),
                         ('activity_id', '=', activity_id.id)])
                    os.system("echo '%s'" % (calendar_attendance))

                    if calendar_attendance:
                        entrada_calendario = str(calendar_attendance.hour_from)
                        salida_calendario = str(calendar_attendance.hour_to)
                        os.system("echo '%s'" % (str(calendar_attendance.hour_from)))
                        os.system("echo '%s'" % (str(calendar_attendance.hour_to)))
                        os.system("echo '%s'" % (str(activity_id.id)))

                        self.ensure_one()
                        db = sql_db.db_connect(self.basedatos)
                        with api.Environment.manage(), db.cursor() as cr:
                            envir = api.Environment(cr, SUPERUSER_ID, {})
                            os.system("echo '%s'" % ('--------conectando con db externa------------------'))

                            user_attendances = envir['marca'].search(
                                [('user_id.user_id', '=', employee.identification_id or employee.identification_id[::-1]),
                                 ('activity_id.name', '=', activity_id.name),
                                 ('timestamp', '>', timestamp_min_str),
                                 ('timestamp', '<', timestamp_max_str),
                                 ], order='timestamp')

                            entrada = ""
                            salida = ""

                            marcas = [(4, line.id) for line in user_attendances]

                            os.system("echo '%s'" % (marcas))
                            checkin = []
                            checkout = []
                            entrada_str = ""
                            salida_str = ""
                            todaslasmarcas = ""
                            for marca in user_attendances:
                                todaslasmarcas += self.fecha_a_hora_str(marca.timestamp) + " "

                                if marca.type == 'checkin':
                                    checkin.append(marca)
                                if marca.type == 'checkout':
                                    checkout.append(marca)
                            os.system("echo '%s'" % (todaslasmarcas))

                            num_checkin = len(checkin)
                            num_checkout = len(checkout)

                            if num_checkin > 0:
                                entrada = checkin[0].timestamp
                                entrada_str = self.fecha_a_hora_str(entrada)
                            if num_checkout > 0:
                                salida = checkout[num_checkout - 1].timestamp
                                salida_str = self.fecha_a_hora_str(salida)

                           # if num_checkin = 0 and num_checkout >0:
                            #    entrada =

                            os.system("echo '%s'" % (timestamp_max_str))
                            os.system("echo '%s'" % (employee.id))
                            os.system("echo '%s'" % (activity_id.id))
                            os.system("echo '%s'" % (marcas))
                            os.system("echo '%s'" % (entrada))
                            os.system("echo '%s'" % (salida))



                            asistencia_mtpe.create({
                                'fecha': timestamp_max_str,
                                'employee_id': employee.id,
                                'activity_id': activity_id.id,
                                'marcas' : todaslasmarcas,
                                'entrada': entrada,
                                'salida': salida,

                            })

                    else:
                        os.system("echo '%s'" % ("no tiene horario ese dia"))

    def generar_data7(self):
        fecha_fin = datetime.strptime(str(self.fecha_fin), "%Y-%m-%d")
        fecha_inicio = datetime.strptime(str(self.fecha_inicio), "%Y-%m-%d")

        dias_feriados = self.env['hr.holidays.public.line']

        for n in range(int((fecha_fin - fecha_inicio).days)):
            fecha_buscar = fecha_inicio + timedelta(n)
            dia_feriado = dias_feriados.search([('date','=', fecha_buscar)])

            if not dia_feriado:
                self.generar7(fecha_buscar)

            else:
                os.system("echo '%s'" % ("FERIADO"))

    @api.multi
    def generar7(self, pub_date):

        employee_ids = self.env['hr.employee'].search([('contract_id.state', "=", 'draft')])  # contratos vigentes
        activity_ids = self.env['attendance.activity'].search([])
        user_attendance_ids = self.env['marca']
        # calendar = self.env['resource.calendar']
        asistencia_mtpe = self.env['asistencia.mtpe']

        os.system("echo '%s'" % ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))
        os.system("echo '%s'" % (pub_date))
        min_pub_date_time = datetime.combine(pub_date, time.min)
        max_pub_date_time = datetime.combine(pub_date, time.max)

        timestamp_min_str = min_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        timestamp_max_str = max_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")

        posicion_dia = min_pub_date_time.weekday()

        os.system("echo '%s'" % (posicion_dia))

        fecha_calculo = self.fecha_local(timestamp_min_str)
        for employee in employee_ids:

            if employee.contract_id.state == 'open':
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))

                for activity_id in activity_ids:
                    os.system("echo '%s'" % (activity_id.name))
                    calendar = employee.contract_id.resource_calendar_id
                    calendar_attendance = self.env['resource.calendar.attendance'].search(
                        [('calendar_id', '=', calendar.id),
                         ('dayofweek', '=', posicion_dia),
                         ('activity_id', '=', activity_id.id)])
                    os.system("echo '%s'" % (calendar_attendance))

                    if calendar_attendance:
                        entrada_calendario = calendar_attendance.hour_from
                        salida_calendario = calendar_attendance.hour_to


                        antes_entrar = calendar_attendance.calendar_id.antes_entrar
                        despues_entrar = calendar_attendance.calendar_id.despues_entrar
                        antes_salir = calendar_attendance.calendar_id.antes_salir
                        despues_salir = calendar_attendance.calendar_id.despues_salir

                        timestamp_entrada_inicio = min_pub_date_time + timedelta(hours= entrada_calendario - antes_entrar)
                        timestamp_entrada_fin = min_pub_date_time + timedelta(hours=entrada_calendario + despues_entrar)

                        timestamp_salida_inicio = min_pub_date_time + timedelta(hours=salida_calendario - antes_salir)
                        timestamp_salida_fin = min_pub_date_time + timedelta(hours=salida_calendario + despues_salir)

                        timestamp_entrada_inicio_str = self.fecha_local(str(timestamp_entrada_inicio.strftime("%Y-%m-%d %H:%M:%S")))
                        timestamp_salida_fin_str = self.fecha_local(str(timestamp_salida_fin.strftime("%Y-%m-%d %H:%M:%S")))

                        os.system("echo '%s'" % (timestamp_entrada_inicio))
                        os.system("echo '%s'" % (timestamp_entrada_fin))
                        os.system("echo '%s'" % (timestamp_salida_inicio))
                        os.system("echo '%s'" % (timestamp_salida_fin))
                        os.system("echo '%s'" % (timestamp_entrada_inicio_str))
                        os.system("echo '%s'" % (timestamp_salida_fin_str))

                        self.ensure_one()
                        db = sql_db.db_connect(self.basedatos)
                        with api.Environment.manage(), db.cursor() as cr:
                            envir = api.Environment(cr, SUPERUSER_ID, {})
                            os.system("echo '%s'" % ('--------conectando con db externa------------------'))


                            marcas_turno = envir['marca'].search(
                                [('user_id.user_id', '=',employee.identification_id or employee.identification_id[::-1]),
                                 ('timestamp', '>', str(timestamp_entrada_inicio_str)),
                                 ('timestamp', '<', str(timestamp_salida_fin_str)),
                                 ], order='timestamp')

                            os.system("echo '%s'" % ("_____________"))
                            os.system("echo '%s'" % (timestamp_entrada_inicio_str))
                            os.system("echo '%s'" % (timestamp_salida_fin_str))
                            os.system("echo '%s'" % (marcas_turno))
                            os.system("echo '%s'" % ("_____________"))
                            checkin = []
                            checkout = []

                            todaslasmarcas = ""

                            for marca in marcas_turno:
                                os.system("echo '%s'" % (self.fecha_a_hora_str(marca.timestamp)))
                                os.system("echo '%s'" % (entrada_calendario+despues_entrar))
                                os.system("echo '%s'" % (salida_calendario - antes_salir))
                                os.system("echo '%s'" % (self.fecha_a_hora_float(marca.timestamp)))
                                os.system("echo '%s'" % (self.fecha_a_hora_float(marca.timestamp)))
                                if self.fecha_a_hora_float(marca.timestamp)< entrada_calendario+despues_entrar:
                                    checkin.append(marca)
                                if self.fecha_a_hora_float(marca.timestamp) > salida_calendario - antes_salir:
                                    checkout.append(marca)


                            num_checkin = len(checkin)
                            num_checkout = len(checkout)

                            entrada = ""
                            salida = ""
                            entrada_str = ""
                            salida_str = ""

                            if num_checkin > 0:
                                entrada = checkin[0].timestamp

                            if num_checkout > 0:
                                salida = checkout[num_checkout - 1].timestamp




                            os.system("echo '%s'" % (timestamp_max_str))
                            os.system("echo '%s'" % (activity_id.id))
                            os.system("echo '%s'" % (entrada))
                            os.system("echo '%s'" % (salida))

                            asistencia_mtpe = self.env['asistencia.mtpe']

                            asistencia_mtpe.create({
                                'fecha': timestamp_max_str,
                                'employee_id': employee.id,
                                'activity_id': activity_id.id,
                                'marcas': todaslasmarcas,
                                'entrada': entrada,
                                'salida': salida,

                            })

                    else:
                        os.system("echo '%s'" % ("no tiene horario ese dia"))

    def generar_data8(self):
        fecha_fin = datetime.strptime(str(self.fecha_fin), "%Y-%m-%d")
        fecha_inicio = datetime.strptime(str(self.fecha_inicio), "%Y-%m-%d")

        min_pub_date_time = datetime.combine(fecha_inicio, time.min)
        max_pub_date_time = datetime.combine(fecha_fin, time.max)

        timestamp_min_str = min_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        timestamp_max_str = max_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        #primero borrar la anterior data
        asistencia_mtpe = self.env['asistencia.mtpe']
        for employee in self.empleados_ids:

            asistencia_pasadas = asistencia_mtpe.search(
            [('employee_id', '=', employee.id), ('fecha', '>=', timestamp_min_str), ('fecha', '<=', timestamp_max_str)])

            os.system("echo '%s'" % ("Asistencia pasadas para borrar"))
            os.system("echo '%s'" % (asistencia_pasadas))

            for asistencia_pasada in asistencia_pasadas:
                asistencia_pasada.unlink()

        dias_feriados = self.env['hr.holidays.public.line']

        for n in range(int((fecha_fin - fecha_inicio).days)+1):
            fecha_buscar = fecha_inicio + timedelta(n)
            dia_feriado = dias_feriados.search([('date','=', fecha_buscar)])

            if not dia_feriado:
                self.generar8(fecha_buscar)

            else:
                os.system("echo '%s'" % ("FERIADO"))

    @api.multi
    def generar8(self, pub_date):

        #employee_ids = self.env['hr.employee'].search([('contract_id.state', "=", 'draft')])  # contratos vigentes
        activity_ids = self.env['attendance.activity'].search([])


        os.system("echo '%s'" % ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))
        os.system("echo '%s'" % (pub_date))
        min_pub_date_time = datetime.combine(pub_date, time.min)
        max_pub_date_time = datetime.combine(pub_date, time.max)

        timestamp_min_str = min_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        timestamp_max_str = max_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        posicion_dia = min_pub_date_time.weekday()
        os.system("echo '%s'" % (timestamp_min_str))
        os.system("echo '%s'" % (timestamp_max_str))
        os.system("echo '%s'" % (posicion_dia))


        for employee in self.empleados_ids:

            if employee.contract_id.state == 'open':
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))



                #se captura la data del dia completo de otra base

                self.ensure_one()
                db = sql_db.db_connect(self.basedatos)
                with api.Environment.manage(), db.cursor() as cr:
                    envir = api.Environment(cr, SUPERUSER_ID, {})
                    os.system("echo '%s'" % ('--------conectando con db externa------------------'))

                    user_attendances = envir['marca'].search(
                        [('user_id.user_id', '=', employee.identification_id or employee.identification_id[::-1]),
                         ('timestamp', '>', timestamp_min_str),
                         ('timestamp', '<', timestamp_max_str),
                         ], order='timestamp')

                    falto_dia= False

                    for activity_id in activity_ids:
                        os.system("echo '%s'" % (activity_id.name))
                        calendar = employee.contract_id.resource_calendar_id
                        calendar_attendance = self.env['resource.calendar.attendance'].search(
                            [('calendar_id', '=', calendar.id),
                             ('dayofweek', '=', posicion_dia),
                             ('activity_id', '=', activity_id.id)])
                        os.system("echo '%s'" % (calendar_attendance))

                        if calendar_attendance:
                            entrada_calendario = str(calendar_attendance.hour_from)
                            salida_calendario = str(calendar_attendance.hour_to)
                            os.system("echo '%s'" % (str(calendar_attendance.hour_from)))
                            os.system("echo '%s'" % (str(calendar_attendance.hour_to)))
                            os.system("echo '%s'" % (str(activity_id.id)))


                            entrada = ""
                            salida = ""

                            marcas = [(4, line.id) for line in user_attendances]

                            os.system("echo '%s'" % (marcas))
                            checkin = []
                            checkout = []
                            entrada_str = ""
                            salida_str = ""
                            todaslasmarcas = ""
                            a = 1
                            b = 1
                            for marca in user_attendances:

                                if self.esta_intervalo(self.fecha_a_hora_float(marca.timestamp),calendar_attendance.hour_from,a,b):
                                    checkin.append(marca)
                                    os.system("echo '%s'" % ("ENTRADA"))
                                    os.system("echo '%s'" % (self.fecha_a_hora_float(marca.timestamp)))

                                elif self.esta_intervalo(self.fecha_a_hora_float(marca.timestamp),calendar_attendance.hour_to,a,b):
                                    checkout.append(marca)
                                    os.system("echo '%s'" % ("SALIDA"))
                                    os.system("echo '%s'" % (self.fecha_a_hora_float(marca.timestamp)))

                                if self.esta_rango(self.fecha_a_hora_float(marca.timestamp),calendar_attendance.hour_from,a,calendar_attendance.hour_to,b):
                                    todaslasmarcas += self.fecha_a_hora_str(marca.timestamp) + " "

                            os.system("echo '%s'" % (todaslasmarcas))

                            num_checkin = len(checkin)
                            num_checkout = len(checkout)

                            if num_checkin > 0:
                                entrada = checkin[num_checkin - 1].timestamp #la ultima marca registrada
                                entrada_str = self.fecha_a_hora_str(entrada)
                            if num_checkout > 0:
                                salida = checkout[num_checkout - 1].timestamp #la ultima marca registrada
                                salida_str = self.fecha_a_hora_str(salida)

                            os.system("echo '%s'" % (timestamp_max_str))
                            os.system("echo '%s'" % (employee.id))
                            os.system("echo '%s'" % (activity_id.id))
                            os.system("echo '%s'" % (marcas))
                            os.system("echo '%s'" % (entrada))
                            os.system("echo '%s'" % (salida))

                            asistencia_mtpe = self.env['asistencia.mtpe']
                            mtpe_obj = asistencia_mtpe.create({
                                'fecha': timestamp_max_str,
                                'employee_id': employee.id,
                                'activity_id': activity_id.id,
                                'marcas' : todaslasmarcas,
                                'entrada': entrada,
                                'salida': salida,

                            })

                            falta = self.env['hr.holidays']

                            if not entrada and not salida: #generar falta


                                os.system("echo '%s'" % ("no hay asistencia"))
                                if activity_id.code == 0 :
                                    os.system("echo '%s'" % ("crear falta"))
                                    tipo_falta = self.env['hr.holidays.status'].search([('codigo', "=", '07')])
                                    falto_dia = True
                                    mtpe_obj.inasistencia = falta.create({ # crear inasistencia en el objeto mtpe
                                        'name': 'falta no justificada',
                                        'holiday_type': 'employee',
                                        'holiday_status_id': tipo_falta.id,
                                        'date_from': self.convert_time_to_utc(timestamp_min_str,'America/Lima'),
                                        'date_to': self.convert_time_to_utc(timestamp_max_str,'America/Lima'),
                                        'report_note': 'Falta',
                                        'number_of_days_temp': 1,
                                        'employee_id': employee.id
                                    })

                                elif not falto_dia and not employee.contract_id.resource_calendar_id.horario_oficina:
                                    tipo_falta = self.env['hr.holidays.status'].search([('codigo', "=", '99')])
                                    mtpe_obj.inasistencia = falta.create({  # crear inasistencia en el objeto mtpe
                                        'name': 'Falta Tarde',
                                        'holiday_type': 'employee',
                                        'holiday_status_id': tipo_falta.id,
                                        'date_from': self.convert_time_to_utc(timestamp_min_str, 'America/Lima'),
                                        'date_to': self.convert_time_to_utc(timestamp_max_str, 'America/Lima'),
                                        'report_note': 'Falta tarde',
                                        'number_of_days_temp': 1,
                                        'employee_id': employee.id
                                    })


                        else:
                            os.system("echo '%s'" % ("no tiene horario ese dia"))



    def fecha_a_hora(self):
        timestamp_str = fields.Datetime.to_string(self.fecha_calculo)
        utc_timestamp_str = self.convert_time_to_utc(timestamp_str, self.env.user.tz)

        start_dt = datetime.strptime(str(utc_timestamp_str), "%Y-%m-%d %H:%M:%S")
        tiempo = start_dt.time()
        hora = tiempo.hour + tiempo.minute / 60
        os.system("echo '%s'" % (utc_timestamp_str))
        os.system("echo '%s'" % (start_dt))
        os.system("echo '%s'" % (hora))

    def fecha_a_hora_str(self,fecha):
        fecha_local = self.convert_utc_time_to_tz(fecha,'America/Lima')
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        return fecha_obj.strftime("%H:%M")

    def fecha_a_hora_float(self,fecha):
        fecha_local = self.convert_utc_time_to_tz(fecha, 'America/Lima')
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        tiempo = fecha_obj.time()
        hora = tiempo.hour + tiempo.minute / 60
        return hora

    def fecha_local(self,fecha):
        return self.convert_utc_time_to_tz(fecha,'America/Lima')

    def esta_intervalo(self,hora,centro,a,b):
        if hora >= (centro - a) and hora <= (centro +b):
            return True
        else:
            return False

    def esta_rango(self,hora,izq,a,der,b):
        if hora >= (izq - a) and hora <= (der +b):
            return True
        else:
            return False


    def get_marcas(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([()])
        for asistencia in asistencia_mtpe:
            asistencia.get_marcas()

    def crear_asistencia(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([('falta', '=', True) ])
        for asistencia in asistencia_mtpe:
            asistencia.crear_asistencia()

    def crear_inasistencia(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([('falta', '=', True) ])
        for asistencia in asistencia_mtpe:
            asistencia.crear_inasistencia()

#lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]
