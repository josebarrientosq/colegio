# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug.utils
from odoo.addons.web.controllers.main import Database
import unicodedata
import os
import base64

class controlador_asistencia(http.Controller):

    fecha_inicio= None
    fecha_fin = None
    nombre = None
    dni=None

    @http.route('/docente', auth='public')
    def login(self, **kwargs):
        return request.render('hr_asistencia.login')

    @http.route('/autenticando', type='http', auth='public', method=["POST"], csrf=False , website=True)

    def autenticar(self, **post):
        dni = post.get("dni")
        password = post.get("password")

        empleado= request.env["hr.employee"].sudo().search([('barcode','=',password)])


        if empleado :
            nombre = empleado.name
            if empleado.barcode == password:
                return http.request.render('hr_asistencia.form',{'nombre' : nombre , 'dni' : dni})
            else:
                return 'clave'
        else:
            return 'dni'

    @http.route('/asistencia', type='http', auth='public', method=["POST"], csrf=False)
    def get_asistencia(self, **post):
        fecha_inicio=post.get("fecha_inicio")
        fecha_fin = post.get("fecha_fin")
        dni = post.get("dni")

        asistencia = request.env["asistencia.mtpe"].sudo().search([('fecha','>=', fecha_inicio),('fecha','<=',fecha_fin),('employee_id.identification_id', '=', dni)])
        os.system("echo '%s'" % ("controlador asistencia"))

        return request.render('hr_asistencia.asistencia',{'asistencia' : asistencia})

    @http.route('/faltas', type='http', auth='public', method=["POST"], csrf=False)
    def get_faltas(self, **post):
        self.fecha_inicio=post.get("fecha_inicio")
        self.fecha_fin = post.get("fecha_fin")
        falta = request.env["hr.holidays"].sudo().search([('date_from','>=', self.fecha_inicio),('date_from','<=',self.fecha_fin)])
        #account_invoice_log = documento.account_log_status_ids[len(documento.account_log_status_ids)-1]
        identificador = ""
        nombre = ""
        #if account_invoice_log.exists():
        #    identificador = account_invoice_log.api_request_id
        #    nombre = account_invoice_log.name

        return request.render('hr_asistencia.faltas',{'falta' : falta,"identificador":identificador,"nombre":nombre})



    @http.route('/reclamo', type='http', auth='public', method=["POST"], csrf=False)
    def reclamo(self, **post):
        id=post.get("id")

        anexo = post.get("anexo")
        article_1 = unicodedata.normalize('NFKD', anexo).encode('ascii', 'ignore')
        article_2 = article_1.lstrip('data:image/jpeg;base64,')

        asistencia = request.env["hr.attendance"].sudo().search([('id', '=', id) ])
        asistencia.reclamo = post.get("reclamo")
        asistencia.reclamo = '/' + article_2

        identificador = ""
        nombre = ""

        asistencia= request.env["hr.attendance"].sudo().search([('check_in','>=',self.fecha_inicio),('check_in','<=',self.fecha_fin)])
        return request.render('hr_asistencia.asistencia',{'asistencia' : asistencia,"identificador":identificador,"nombre":nombre})

    @http.route('/asistencia/attachment/add', type='http', auth="public", website=True)
    def asistencia_attachment_add(self, url=None, upload=None, **post):
        os.system("echo '%s'" % ("----adddd----"))
        cr, uid, context = request.cr, request.uid, request.context
        asistencia_id = post.get("asistencia_id")
        os.system("echo '%s'" % (asistencia_id))
        asistencia_id = request.env["asistencia.mtpe"].sudo().search([('id', '=', asistencia_id)])

        motivo = post.get("motivo")
        asistencia_id.motivo = motivo

        Attachments = request.env['ir.attachment']  # registry for the attachment table

        upload_file = request.httprequest.files.getlist('anexo')
        # data = upload_file.read()
        if upload_file:
            for i in range(len(upload_file)):
                attachment_id = Attachments.create({
                    'name': upload_file[i].filename,
                    'type': 'binary',
                    'datas': base64.b64encode(upload_file[i].read()),
                    'datas_fname': upload_file[i].filename,
                    'public': True,
                    'res_model': 'ir.ui.view',
                    'asistencia_attachment_id': asistencia_id.id,
                })

            return {}

    @http.route('/asistencia/attachment/justificacion', type='http', auth="public", website=True, csrf=False)
    def justificacion_attachment_add(self, url=None, upload=None, **post):
        os.system("echo '%s'" % ("----justificacion----"))
        os.system("echo '%s'" % ("enviando justiicacion"))
        cr, uid, context = request.cr, request.uid, request.context
        justificacion_id = post.get("justificacion_id")
        os.system("echo '%s'" % (justificacion_id))
        justificacion_id = request.env["asistencia.mtpe"].sudo().search([('id', '=', justificacion_id)])

        motivo = post.get("motivo")
        justificacion_id.motivo = motivo

        Attachments = request.env['ir.attachment']  # registry for the attachment table

        upload_file = request.httprequest.files.getlist('anexo')
        os.system("echo '%s'" % (upload_file))
        # data = upload_file.read()
        if upload_file:
            for i in range(len(upload_file)):
                attachment_id = Attachments.create({
                    'name': upload_file[i].filename,
                    'type': 'binary',
                    'datas': base64.b64encode(upload_file[i].read()),
                    'datas_fname': upload_file[i].filename,
                    'public': True,
                    'res_model': 'ir.ui.view',
                    'asistencia_attachment_id': justificacion_id.id,
                })

            return {}


    @http.route('/justificacion/attachment/add', type='http', auth="public",method=["POST"], csrf=False, website=True)
    def empleado_attachment_add(self, url=None, upload=None, **post):

        cr, uid, context = request.cr, request.uid, request.context
        nombre = post.get("nombre")
        imagen = post.get("imagen")

        asistencia_id = post.get("justificacion_id")
        os.system("echo '%s'" % (asistencia_id))
        asistencia = request.env["asistencia.mtpe"].sudo().search([('id', '=', asistencia_id)])

        motivo = post.get("motivo")
        asistencia.motivo = motivo


        Attachments = request.env['ir.attachment']  # registry for the attachment table


        attachment_id = Attachments.create({
            'name': "base64archivo",
            'type': 'binary',
            'datas': imagen,
            'datas_fname': "base64archivo",
            'public': True,
            'res_model': 'ir.ui.view',
            'asistencia_attachment_id': asistencia.id,
        })

        return {}