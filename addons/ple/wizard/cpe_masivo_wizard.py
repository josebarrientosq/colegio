from odoo import models, fields, api


class Aceptar_letras_wizard(models.TransientModel):
    _name = 'cpe_masivo_wizard'
    _description = "consulta cpe masivo"


    @api.multi
    def exportar(self):

        file_data = self._get_txt()
        #LERRRRRRRRRRRAAAAMM0014010000OIM1.TXT
        file_name = "consulta cpe masivo.txt"
        values = {
            'name': file_name,
            'datas_fname': file_name,
            'res_model': 'ir.ui.view',
            'res_id': False,
            'type': 'binary',
            'public': True,
            'datas' : codecs.encode(codecs.encode(file_data,'utf8'),'base64'),

        }
        attachment_id = self.env['ir.attachment'].sudo().create(values)
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
        }

    @api.multi
    def _get_txt(self):

        txt=""
        #documents = self.env['account.invoice'].search([ ('date_invoice', '>', self.fecha_ini) , ('date_invoice','<',self.fecha_fin)  ])
        documents = self.cpe_ids

        for document in documents.linea:
            txt += self.company_id.partner_id.vat + "|"
            txt += document.journal_id.invoice_type_code_id + "|"
            txt += document.number[:4] + "|"
            txt += document.doc.number[8:] + "|"
            txt += modificar_fecha(document.date_invoice) + "|"
            txt += str(document.amount_total)
            txt += "\r\n"

        return txt




    def _get_cpe(self):
        if self.env.context and self.env.context.get('active_ids'):
            return self.env.context.get('active_ids')
        return []


    cpe_ids = fields.Many2many('account.invoice', default=_get_cpe, string='cpe')
    fecha_ini = fields.Date("fecha inicio")
    fecha_fin = fields.Date("Fecha fin")

    @api.multi
    def exportar_cpe(self):
        active_ids = self.env.context.get('active_ids', []) or []
        records = self.env['account.invoice'].browse(active_ids)
        self.exportar


def modificar_fecha(fecha):
    if fecha:
        fecha = str(fecha)
        return fecha[8:10] + "/" + fecha[5:7] + "/" + fecha[0:4]
    else:
        return ""


def get_serie(number):
    if number:
        return number[0:4]
    else:
        return ""


def get_correlativo(number):
    if number:
        return number[8:]
    else:
        return ""
