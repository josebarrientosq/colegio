from odoo import fields,models,api
from odoo import exceptions, _
from odoo.exceptions import ValidationError, UserError
import datetime , codecs

class ple_ventas (models.Model):
    _name = "ple.ventas_diarias"

    day_init = fields.Date(string='inicio')
    day_end = fields.Date(string='fin')
    linea = fields.One2many('ple.ventas_diarias_linea', 'base_id', string="detalle de reporte")
    state = fields.Selection(selection=[
        ('draft', 'Borrador'),
        ('generated', 'Generado'),
        ('exported', 'Exportado'),
    ], default='draft')

    @api.multi
    def imprimir(self):
        self.ensure_one()
        return self.env.ref('ple.report_ventas_14_1').report_action(self)

    @api.one
    def recargar(self):
        self.env["ple.ventas_diaria_linea"].search([("day", ">=", self.day_init)]).unlink()

        self.write({'state': 'generated'})
        linea_obj = self.env['ple.ventas_linea']
        docs = self.env["account.invoice"].search([("date_invoice",">=",self.day_init),("date_invoice","<=",self.day_end),("state","!=","draft")])
        for li in self.browse(self.ids):
            for doc in docs:
                linea_obj.create({
                    'base_id': li.id,
                    'day' : fields.Date(string='fin'),
                    'documentos' : fields.Char(),
                    'total_day' : fields.Float(),


                })
        return True

    @api.multi
    def exportar(self):
        self.write({'state': 'exported'})
        #self.nro_generados = self.nro_generados + 1
        file_data = self._get_txt()
        file_name = "ventas diarias desde " + self.date_init +" al "+ self.date_end+ ".txt"
        values = {
            'name': file_name,
            'datas_fname': file_name,
            'res_model': 'ir.ui.view',
            'res_id': False,
            'type': 'binary',
            'public': True,
            'datas' : codecs.encode(codecs.encode(file_data,'utf8'),'base64'),

        }
        attachment_id = self.env['ir.attachment'].sudo().create(values)
        download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
            "type": "ir.actions.act_url",
            "url": str(base_url) + str(download_url),
            "target": "new",
        }

    @api.multi
    def _get_txt(self):
        i=1
        txt=""
        documents = self.env['ple.ventas'].search([ ['period_id.code', '=', self.period_id.code] ])

        if len(documents)>1:
            raise UserError("Periodo repetido")

        for document in documents.linea:

            txt += modificar_fecha_periodo(document.period_id_linea.code)+ "|" + document.cuo + "|M"+document.cuo+ "|" + modificar_fecha(document.date_invoice) + "|" + modificar_fecha(document.date_due) + "|" + document.journal_code_name + "|" + document.serie + "|" + document.correlativo+"|"+"|"

            i += 1
        return txt

def modificar_fecha_periodo(periodo):
    if periodo:
        fecha = str(periodo)
        return periodo[3:7]+periodo[0:2]+"00"
    else:
        return ""

def modificar_fecha(fecha):
        if fecha:
            fecha = str(fecha)
            return fecha[8:10] + "/" + fecha[5:7] + "/" + fecha[0:4]
        else:
            return ""

def get_serie(number):
    if number:
        return number[0:4]
    else:
        return ""

def get_correlativo(number):
    if number:
        return number[8:]
    else:
        return ""



class ple_compras_linea(models.Model):
    _name = 'ple.ventas_diarias_linea'

    day = fields.Date(string='fin')
    documentos = fields.Char()
    total_day = fields.Float()

    base_id = fields.Many2one('ple.ventas_diarias')


