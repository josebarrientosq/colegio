# -*- coding: utf-8 -*-
{
    'name': "Emisión de Facturas Electrónicas",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "DANIEL MORENO",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','base_setup','sale','stock','sale_stock','account','odoope_einvoice_base','odoope_toponyms','sunat_get_data'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'config/res_config_settings.xml',
        'data/product_uom.xml',
        'data/account_journal.xml',
        'data/account_tax_group.xml',
        'data/res_currency.xml',
        'security/res_groups.xml',
        'views/company/view_company.xml',
        'views/account/view_account_log_status.xml',
        'views/partner/view_res_partner.xml',
        'views/product/view_product_uom.xml',
        'views/account/view_account_journal.xml',
        'views/account/view_acc_inv_boleta.xml',
        'views/account/view_acc_inv_factura.xml',
        'views/account/view_acc_inv_nota_credito.xml',
        'views/account/view_acc_inv_nota_debito.xml',
        'views/sale_order/sale_order.xml',
        'views/account/view_account_invoice.xml',
        'views/account/view_account_tax.xml',
        'views/account/view_acc_com_baja.xml',
        'views/stock/stock_picking.xml',
        
    ],
    'external_dependencies':{"python":["jwt"]},
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}