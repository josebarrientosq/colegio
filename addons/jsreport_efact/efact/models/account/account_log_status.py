from odoo import api,models,fields


class AccountLogStatus(models.Model):
    _name = "account.log.status"

    name = fields.Char("Nombre")
    api_user_id = fields.Char("API User id")
    content_xml = fields.Char("Respuesta de SUNAT base64 ZIP")

    response_xml = fields.Char("Respuesta de SUNAT")
    request_json = fields.Char("Contenido de Comprobante en JSON")
    signed_xml = fields.Char("Envío de comprobante ZIP en base64")
    response_json = fields.Char("Respuesta del API")
    unsigned_xml = fields.Char("Comprobante sin Firmar")
    signed_xml_data = fields.Char("Comprobante XML Firmado")
    status = fields.Char("Estado de envío a SUNAT")
    date_request = fields.Date("Fecha de Envío al API")
    date_issue = fields.Date("Fecha de Emisión a SUNAT")
    api_request_id = fields.Char("Identificador de Envío")

    account_invoice_id = fields.Many2one("account.invoice",string="Comprobante")

    
