# -*- coding: utf-8 -*-

from odoo import fields,models,api

class AccountTaxGroup(models.Model):
    _inherit = "account.tax.group"

    code = fields.Char("Código",required=True)
    description = fields.Char("Descripción",required=True)
    name_code = fields.Char("Nombre del Código",required=True)
