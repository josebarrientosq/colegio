# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
{
    "name" : "Website File Upload(Upload Attachment)",
    "version" : "11.0.0.3",
    "category" : "eCommerce",
    "depends" : ['website','website_sale','hr_planilla'],
    "author": "BrowseInfo",
    "price": 20,
    "currency": 'EUR',
    'summary': 'This apps helps to upload file/Attachment on Odoo Website',
    "description": """
    
    Purpose :- 
    Odoo Website File Upload.
    Odoo website attachment upload, odoo website document upload
    Website attach document, website file attachement, website file upload
    website upload attachment, website upload document, website upload file, File upload on website, Attachment upload on website, Document upload on website
    """,
    "website" : "www.browseinfo.in",
    "data": [
        'views/views.xml',
        'views/template.xml',
    ],
    "auto_install": False,
    "application": True,
    "installable": True,
    "live_test_url":'https://www.youtube.com/watch?v=Qqh2Oir8nNw&feature=youtu.be',
    "images":["static/description/Banner.png"],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
