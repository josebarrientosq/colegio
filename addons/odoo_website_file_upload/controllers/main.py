# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import odoo.http as http
from odoo.http import request
import base64

class OdooWebsiteFileUpload(http.Controller):

    @http.route('/payslip/attachment/add', type='http', auth="public", website=True)
    def payslip_attachment_add(self, url=None, upload=None, **post):
    
        cr, uid, context = request.cr, request.uid, request.context
        number_rh = post.get("number_rh")
        #order = request.website.sale_get_order()
        payslip = request.env["hr.payslip"].sudo().search([('number', '=', number_rh)])

        Attachments = request.env['ir.attachment']  # registry for the attachment table
        
        upload_file = request.httprequest.files.getlist('upload')
        # data = upload_file.read()
        if upload_file: 
            for i in range(len(upload_file)):
                attachment_id = Attachments.create({
                    'name': upload_file[i].filename,
                    'type': 'binary',
                    'datas': base64.b64encode(upload_file[i].read()),
                    'datas_fname': upload_file[i].filename,
                    'public': True,
                    'res_model': 'ir.ui.view',
                    'payslip_attachment_id' : payslip.id,
                })   
        
            return request.redirect('/planilla')

    @http.route('/rh/attachment/add', type='http', auth="public", website=True)
    def rh_attachment(self,url=None,number_rh=None, archivo_rh=None, **post):
        number = 'BP-00000005'

        payslip = request.env["hr.payslip"].sudo().search([('number', '=', number)])

        Attachments = request.env['ir.attachment']  # registry for the attachment table

        upload_file = request.httprequest.files.getlist('archivo_rh')
        # data = upload_file.read()
        if upload_file:
            for i in range(len(upload_file)):
                attachment_id = Attachments.create({
                    'name': upload_file[i].filename,
                    'type': 'binary',
                    'datas': base64.b64encode(upload_file[i].read()),
                    'datas_fname': upload_file[i].filename,
                    'public': True,
                    'res_model': 'ir.ui.view',
                    'payslip_attachment_id': payslip.id,
                })

            return request.redirect('/planilla')

