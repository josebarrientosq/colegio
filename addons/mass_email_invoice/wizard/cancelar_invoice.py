# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from openerp import models, fields, api
from odoo.exceptions import ValidationError, UserError


class WizardOpStudent(models.TransientModel):
    _name = 'wizard_cancelar_invoice'
    _description = "Eliminar invoice masivo"

    def _get_invoices(self):
        if self.env.context and self.env.context.get('active_ids'):
            return self.env.context.get('active_ids')
        return []

    invoice_ids = fields.Many2many(
        'account.invoice', default=_get_invoices, string='Documentos')

    @api.multi
    def cancelar_invoice(self):
        active_ids = self.env.context.get('active_ids')
        records = self.env['account.invoice'].browse(active_ids)
        for record in records:
            record.action_invoice_cancel()