# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request

class buscador(http.Controller):

    @http.route('/consulta', auth='public')
    def index(self, **kwargs):
        return request.render('buscador.form')

    @http.route('/busqueda', type='http', auth='public', method=["POST"], csrf=False)
    def consulta(self, **post):
        numero= post.get("serie")+"-"+ post.get("correlativo")
        fecha=post.get("fecha")
        ruc=post.get("ruc")
        total=post.get("total")
        documento = request.env["account.invoice"].search([['number',"=",numero],['partner_id.vat',"=",ruc],['date_invoice','=',fecha],['amount_total','=',total]])
        account_invoice_log = documento.account_log_status_ids[len(documento.account_log_status_ids)-1]
        identificador = ""
        nombre = ""
        if account_invoice_log.exists():
            identificador = account_invoice_log.api_request_id
            nombre = account_invoice_log.name

        return request.render('buscador.documentos',{'documento' : documento,"identificador":identificador,"nombre":nombre})

    @http.route("/consulta/comprobante/xml/<identificador>",type='http',method=["GET"],csrf=False)
    def comprobante_xml(self,identificador):
        log = request.env["account.log.status"].search([["api_request_id","=",identificador]])
        xml = log.signed_xml_data
        xml_headers = [
            ('Content-Type', 'application/xml'),
            ('Content-Length', len(xml)),
            ('Content-Disposition', 'attachment; filename="%s.xml"'%(log.name)),
        ]
        return request.make_response(xml, headers=xml_headers)

    @http.route("/consulta/comprobante/pdf/<identificador>",type='http',method=["GET"],csrf=False)
    def comprobante_pdf(self,identificador):
        log = request.env["account.log.status"].search([["api_request_id","=",identificador]])
        invoice = log.account_invoice_id
        if invoice:
            pdf = request.env.ref('account.account_invoices_without_payment').sudo().render_qweb_pdf([invoice.id])[0]
            pdfhttpheaders = [('Content-Type', 'application/pdf'), 
                                ('Content-Length', len(pdf)),
                                ('Content-Disposition', 'attachment; filename="%s.pdf"'%(log.name))]
            return request.make_response(pdf, headers=pdfhttpheaders)
        else:
            return request.redirect('/')


        #    ['|', ["vat", "=", identificador], ["codigo", "=", identificador], ["es_comensal", "=", True]])

#     @http.route('/json/json/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('json.listing', {
#             'root': '/json/json',
#             'objects': http.request.env['json.json'].search([]),
#         })

#     @http.route('/json/json/objects/<model("json.json"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('json.object', {
#             'object': obj
#         })